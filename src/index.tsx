/**
 * Author: Timothy Headrick
 * Descr: Contains all routes for the website and describes structure. If a component needs to act as its own page, it must be place here.
 */
import ReactDOM from "react-dom/client";
import "./styles/index.css";
import App from "./modules/App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ErrorProvider } from "./modules/ErrorContext"; 

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

const queryClient = new QueryClient();

root.render(
  <QueryClientProvider client={queryClient}>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <React.StrictMode>
        <ErrorProvider>
          <App />
        </ErrorProvider>
      </React.StrictMode>
    </BrowserRouter>
  </QueryClientProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
