/**
 * Author: Timothy Headrick
 * Desc: Contains miscellaneous universal application variables.
 */
import { createTheme } from "@mui/material";

//root route for the api. requests are based on this, so changing it allows for changing the host
//location of the api
export const API_ROOT_URL = "http://localhost:8000/api/";
export const AUTH_URL = "http://localhost:8000/auth/";
export const ROOT_URL = "http://localhost:8000/"
export const SLURM_API_URL = "http://localhost:8050/api"
export const SLURM_ROOT_URL = "http://localhost:8050/"
export const ANNOTATED_URL = "http://localhost:8000/annotated-image"

//Used by material ui components to correct font
export const THEME = createTheme({
  typography: {
    fontFamily: `"Orgon Slab", sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
  palette: {
    primary: {
      main: "#005F83",
    },
    secondary: {
      main: "#007A33",
    },
  },
  //   components: {
  //     MuiButton: {
  //       styleOverrides: {
  //         // Name of the slot
  //         root:
  //           // Some CSS
  //           sx({
  //             backgroundColor: "#005F83",
  //             "&:hover": {
  //               backgroundColor: "#007A33",
  //             },
  //           }),
  //       },
  //     },
  //   },
});
