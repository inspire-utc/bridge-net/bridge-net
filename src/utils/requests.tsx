import axios from "axios"

export function post(endpoint, body) {
    let user_info = JSON.parse(sessionStorage.getItem("user-info")!);
    let token = user_info.token;
    let config = {
      headers: {
        Authorization: "Token " + token
      }
    }
    return axios.post(endpoint, body, config);
}
