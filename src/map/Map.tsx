import React from "react"
import MapView from "@arcgis/core/views/MapView";
import Map from "@arcgis/core/Map";

import { useEffect, useRef } from "react"

export default
function MapWrapper() {

  const mapDiv = useRef(null);

  useEffect(() => {
    if (mapDiv.current) {
      /**
       * Initialize application
       */
      const webmap = new Map({
        basemap: "streets-vector"
      });

      const view = new MapView({
        container: mapDiv.current, // The id or node representing the DOM element containing the view.
        map: webmap, // An instance of a Map object to display in the view.
        center: [-117.1490,32.7353],
        scale: 10000000 // Represents the map scale at the center of the view.
      });
      // Disable scroll wheel zooming
      view.on("mouse-wheel", (event) => {
        event.stopPropagation();
      });

      return () => view && view.destroy()
    }
  }, []);

  return <div></div>;
}




