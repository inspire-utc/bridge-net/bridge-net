/**
 * Author: Timothy Headrick
 * Desc: This file defines everything related to the login popup where users can enter their credentials.
 * It relies heavily on materialUI, so its styles can be slightly confusing. While it has css, there
 * are many things that require using the style prop.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "../../styles/LoginPopup.css";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import { Button, FormControl } from "@mui/material";
import axios from "axios";
import { AUTH_URL } from "../../utils/vars";

export interface ILoginPopupProps {
  on: boolean;
  handleOn: () => void;
}

export const LoginPopup = ({ on, handleOn }: ILoginPopupProps) => {
  const [loginInfo, setLoginInfo] = useState({
    username: "",
    password: "",
  }); //where the textInputs keep data
  const [rememberMe, setRememberMe] = useState(false); //for the checkbox of rememberMe
  const [loginFailed, setLoginFailed] = useState(false); //to show the login failed error colors

  //anytime the username textInput changes
  const handleUsername: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setLoginInfo({
      username: event.target.value,
      password: loginInfo.password,
    });
  };

  //anytime the password input changes
  const handlePassword: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setLoginInfo({
      username: loginInfo.username,
      password: event.target.value,
    });
  };

  //anytime the rememberMe checkbox is toggled
  const handleRememberMe: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setRememberMe(event.target.checked);
  };

  //whenever Sign In is pressed, this attempts
  //authentication then stores the token in either sessionStorage or localStorage
  //depening on rememberMe
  const handleSignIn = () => {
    axios
      .post(AUTH_URL, {
        username: loginInfo.username,
        password: loginInfo.password,
      })
      .then((response) => {
        if (rememberMe) {
          localStorage.setItem(
            "user-info",
            JSON.stringify({
              token: response.data.token,
              userId: response.data.user,
            })
          );
        } else {
          sessionStorage.setItem(
            "user-info",
            JSON.stringify({
              token: response.data.token,
              userId: response.data.user,
            })
          );
        }

        handleOn();
      })
      .catch(() => {
        setLoginFailed(true);
      });
  };

  return (
    <>
      {on && (
        <div className="login-background">
          <div className="login-popup">
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
            Because of that tendency, there may be times where the style attribute needs
            to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <FormControl
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label className="login-label-header">Sign In</label>
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                style={{ width: "20rem", height: "2rem", marginTop: "5%" }}
                color="primary"
                type="text"
                value={loginInfo.username}
                onChange={handleUsername}
                label="Username/Email"
                variant="standard"
                error={loginFailed}
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={loginInfo.password}
                onChange={handlePassword}
                type="password"
                label="Password"
                variant="standard"
                error={loginFailed}
                helperText={loginFailed ? "Incorrect Username/Password" : ""}
              />
              <FormGroup row style={{ marginTop: "10%", width: "90%" }}>
                <FormControlLabel
                  value="remember-me"
                  control={
                    <Checkbox
                      checked={rememberMe}
                      onChange={handleRememberMe}
                    />
                  }
                  label="Remember Me"
                  labelPlacement="start"
                />
                <Button
                  variant="contained"
                  style={{ marginLeft: "30%" }}
                  sx={{
                    backgroundColor: "#005F83",
                    "&:hover": {
                      backgroundColor: "#007A33",
                    },
                  }} //like style but support for more things related to MaterialUI
                  onClick={handleSignIn}
                >
                  Sign In
                </Button>
              </FormGroup>
            </FormControl>
          </div>
        </div>
      )}
    </>
  );
};
