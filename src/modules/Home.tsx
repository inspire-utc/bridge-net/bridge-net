/**
 * Author: Timothy Headrick
 * Desc: Currently, this only contains the navbar for the application, but it also contains the "root outlet," which
 * describes where all other parts of the application are rendered since all other parts of the Homelication are
 * children of the Home route.
 */
import { Outlet, Link, useLocation } from "react-router-dom";
import LoginIcon from "@mui/icons-material/Login";
import logo from "../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_W_PH.png";
import learnIcon from "../images/book-16-xxl.png";
import "../styles/Home.css";
import { LoginPopup } from "./login/LoginPopup";
import { useEffect, useState } from "react";
import ThemeProvider from "@mui/material/styles/ThemeProvider";
import { API_ROOT_URL, THEME } from "../utils/vars";
import axios from "axios";


// import { SearchBar } from "./search/SearchBar";

export default function Home() {
  const location = useLocation();
  const [loginActive, setLoginActive] = useState(false);
  const [signedIn, setSignedIn] = useState({ username: "", active: false });

  const toggleLoginActive = (): void => {
    setLoginActive(!loginActive);
  };

  const signOut = (): void => {
    localStorage.removeItem("user-info");
    sessionStorage.removeItem("user-info");
    setSignedIn({ username: "", active: false });
  };

  useEffect(() => {
    const storedInfo =
      localStorage.getItem("user-info") || sessionStorage.getItem("user-info");
    if (storedInfo) {
      axios
        .get(`${API_ROOT_URL}users/${JSON.parse(storedInfo).userId}`, {
          headers: {
            Authorization: `Token ${JSON.parse(storedInfo).token}`,
          },
        })
        .then((response: any) => {
          setSignedIn({ username: response.data.username, active: true });
          sessionStorage.setItem("user-info", storedInfo); //just to make sure the token is always at least in sessionStorage
        })
        .catch(() => {
          console.log("Login credentials expired.");
        });
    }
  }, [loginActive]);

  return (
    <ThemeProvider theme={THEME}>
      <div className="Home">
        <div className="Home-navbar">
          {/* The upper blue part of the navbar*/}
          <div className="Home-navbar-top">
            {/* The CII Logo and link to home ie base Home component*/}
            <Link className="Home-link-navbar-logo" to="/">
              <img className="Home-navbar-logo" src={logo} alt="MST-CII Logo" />
            </Link>
            <div className="Home-navbar-right-icons">
              {/* Show login symbol when not logged in */}
              {!signedIn.active && (
                <LoginIcon
                  className="Home-navbar-login-icon"
                  style={{ fontSize: "2.8rem" }}
                  onClick={toggleLoginActive}
                />
              )}
              {/* Show username when logged in. This should be temporary or modified */}
              {signedIn.active && (
                <p
                  style={{
                    marginLeft: "auto",
                    marginRight: "0.7rem",
                    color: "white",
                    cursor: "pointer",
                  }}
                  onClick={signOut}
                >
                  {signedIn.username}
                </p>
              )}
              {/* The Book Icon and eventually link to teaching how to use the website*/}
              <div className="Home-link-navbar-learn-icon">
                <Link className="Home-link-navbar-learn-icon" to="/learn">
                  <img
                    className="Home-navbar-learn-icon"
                    src={learnIcon}
                    alt="Link to Learn Page"
                  />
                </Link>
                {/*<SearchBar />*/}
              </div>
            </div>
          </div>
          {/* The bottom part of the navbar where the navigation buttons are */}
          <div className="Home-navbar-bottom">
            <Link
              to="/dnn-training"
              className={
                location.pathname.includes("/dnn-training")
                  ? "Home-navbar-bottom-link-active"
                  : "Home-navbar-bottom-link"
              }
            >
              DNN Training{" "}
            </Link>
            <Link
              to="/inspection"
              className={
                location.pathname.includes("/inspection")
                  ? "Home-navbar-bottom-link-active"
                  : "Home-navbar-bottom-link"
              }
            >
              Inspection{" "}
            </Link>
            <Link
              to="/archive/database"
              className={
                location.pathname.includes("/archive")
                  ? "Home-navbar-bottom-link-active"
                  : "Home-navbar-bottom-link"
              }
            >
              Archive
            </Link>
          </div>
        </div>
        <Outlet/>
        <LoginPopup on={loginActive} handleOn={toggleLoginActive} />
      </div>
    </ThemeProvider>
  );
}
