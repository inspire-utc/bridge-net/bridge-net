import { AnimatePresence } from "framer-motion";
import { Route, Routes, useLocation } from "react-router";
import { Navigate } from "react-router-dom";
import Archive from "./archive/Archive";
import BridgesGrid from "./archive/database/BridgesGrid";
import ConditionsGrid from "./archive/database/ConditionsGrid";
import Database from "./archive/database/Database";
import ElementsGrid from "./archive/database/ElementsGrid";
import { BridgeGallery } from "./archive/gallery/bridge-gallery/BridgeGallery";
import { BridgeGallerySimple } from "./archive/gallery/bridge-gallery/BridgeGallerySimple";
import Gallery from "./archive/gallery/Gallery";
import VideoGallery from "./archive/videogallery/VideoGallery";
import Workflow from "./dnn-training/workflow/workflow"
import DNNTraining from "./dnn-training/DNNTraining";
import JobList from "./dnn-training/training-job/JobList";
import Home from "./Home";
import Inspection from "./inspection/Inspection";
import Learn from "./learn/Learn";
import AnnotatorTable from "./dnn-training/annotator/AnnotatorTable";
import Annotator from "./dnn-training/annotator/Annotator";
import TestSetCreator from "./dnn-training/train-set-creator/TestSetCreator";
import WorkflowTable from "./dnn-training/workflow/workflowtable"
import Monitor from "./dnn-training/monitor/monitor"
import { MoneyOffTwoTone } from "@mui/icons-material";
import WorkItemTable from "./dnn-training/monitor/workitem-table/workitem-table";
import JobViewer from "./dnn-training/workflow/jobviewer";
import QueryElementImages from "./inspection/query-element-images"
import MapWrapper from "../map/Map"
import VideoQuery from "./archive/videogallery/VideoQuery";

export default function App() {
  const location = useLocation();
  return (
    <AnimatePresence exitBeforeEnter>
      <Routes key={location.pathname} location={location}>
        <Route path="/" element={<Home />}>
        <Route index element={<MapWrapper />} />
          <Route path="archive" element={<Archive />}>
            <Route path="database" element={<Database />}>
              <Route path="bridges" element={<BridgesGrid />} />
              <Route path="elements" element={<ElementsGrid />} />
              <Route path="conditions" element={<ConditionsGrid />} />
            </Route>
            <Route path="gallery" element={<Gallery />} />
            {/* The ":structurenumber" indicates that this is a dynamic route. See more in BridgeGallery component*/}
            <Route
              path="gallery/:structure_number"
              element={<BridgeGallery />}
            />
            <Route
              path="gallery/:structure_number/min"
              element={<BridgeGallerySimple />}
            />
            <Route path="videogallery" element={<VideoGallery />}>
            </Route>
            <Route path="videoquery" element={<VideoQuery />}></Route>


          </Route>
          <Route path="learn" element={<Learn />} />
          <Route path="inspection" element={<Inspection />}>
          </Route>
          <Route path="dnn-training" element={<DNNTraining />}>
            <Route path="annotations" element={<AnnotatorTable />} />
            <Route path="annotations/:structure_number" element={<Annotator />} />
            <Route path="workflows" element={<WorkflowTable />} />
            <Route path="workflows/:guid" element={<Workflow />} />
            <Route path="monitor" element={<Monitor />}/>
            <Route path="monitor/:guid" element={<JobViewer />}/>
          </Route>
          <Route path="query-element-images/" element={<QueryElementImages />}/>

        </Route>
        <Route path="*" element={<Navigate to="/" />} />
        {/* If current route does not match anything. */}
      </Routes>
    </AnimatePresence>
  );
}
