import { useLocation } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Model } from 'survey-core';
import { Survey as SurveyUI } from 'survey-react-ui';
import * as Survey from 'survey-core';
import 'survey-core/defaultV2.min.css';

function ImagePagePrototype (image: IImageData) {
    return { 
        elements: [ 
        {
            "type": "image",
            "name": "",
            "imageLink": image.file,
            
            "altText": "SurveyJS component integration",
        },
        {
            name: `condition-${image.image_id}`,
            type: "checkbox",
            title: "Condition",
            isRequired: true,
            choices: [ "CS1", "CS2", "CS3", "CS4" ]
        },
        {
            name: `environment-${image.image_id}`,
            type: "checkbox",
            title: "Environment",
            isRequired: true,
            choices: [ "Benign", "Low", "Moderate", "Severe" ]
        },
        {
            name: `defect-${image.image_id}`, 
            type: "radiogroup",
            title: "Defect",
            isRequired: true,
            choices: ["Yes", "No"]
        },
        {
            name: `code-${image.image_id}`,
            type: "text",
            title: "Code",
            inputType: "number",
            isRequired: true,
            choices: []
        }
        ]
    }
}

function createSurveyFromImages(imagedata: IImageData[]) {
    return { 
        pages: imagedata.map(ImagePagePrototype) 
    }
}

interface IImageData {
    file: string;
    bridge: Number;
    element: string;
    image_id: Number;
}


function ShowImageData(props: IImageData) {
    return <div className="ShowImageData">
        <div>Element: {props.element}</div>
        <img src={props.file}/>
    </div>
}


export default function QueryElementImages() {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);

  const bridge = queryParams.get('bridge');
  const element = queryParams.get('element');

  const [surveyJSON, setSurveyJSON] = useState({});

  const query = `http://localhost:8000/api/image-element?bridge=${bridge}&element=${element}`;

  useEffect(() => {
    // Fetch the images inside useEffect
    fetch(query)
      .then((resp) => resp.json())
      .then((result) => {
        let imageData = result.results.map((res) => ({
          file: res.image.file,
          bridge: res.bridge.structure_number,
          element: res.element.name,
          image_id: res.image.id,
        }));
        setSurveyJSON(createSurveyFromImages(imageData));
      })
      .catch((error) => {
        console.error('Error fetching images:', error);
      });
  }, []); // Only re-run the effect if `bridge` or `element` changes



  return (
    <div>
      <SurveyUI model={new Survey.Model(surveyJSON)} /> {/* Ensure you pass a Survey.Model instance to your SurveyUI component */}
    </div>
  );
}