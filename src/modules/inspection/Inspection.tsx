import { ROOT_URL } from "../../utils/vars";
import { Model } from 'survey-core';
import { Survey as SurveyUI } from 'survey-react-ui';
import * as Survey from 'survey-core';
import 'survey-core/defaultV2.min.css';
import { useState } from "react";
import { useNavigate } from "react-router-dom"

const initialSurveyJson = {
  elements: [
    {
      name: "bridge",
      type: "dropdown",
      title: "Select Bridge",
      choicesByUrl: {
        url: "http://localhost:8000/api/bridges",
        path: "results",
        valueName: "structure_number"
      }
    },
    {
      name: "element",
      type: "dropdown",
      title: "Select Element",
      choicesByUrl: {
        url: "http://localhost:8000/api/elements",
        path: "results",
        valueName: "name"
      }
    },
  ]
};

const inspectionSurveyJson = {
  elements: [
    {
      name: "video",
      type: "video",
      title: "Inspection Video",
      videoUrl: "https://www.w3schools.com/html/mov_bbb.mp4",
      isRequired: false,
      choices: []
    },
    {
      name: "condition",
      type: "checkbox",
      title: "Condition",
      isRequired: true,
      choices: [ "CS1", "CS2", "CS3", "CS4" ]
    },
    {
      name: "environment",
      type: "checkbox",
      title: "Environment",
      isRequired: true,
      choices: [ "Benign", "Low", "Moderate", "Severe" ]
    },
    {
      name: "defect", 
      type: "radiogroup",
      title: "Defect",
      isRequired: true,
      choices: ["Yes", "No"]
    },
    {
      name: "code",
      type: "text",
      title: "Code",
      inputType: "number",
      isRequired: true,
      choices: []
    } 
  ]
}

Survey.CustomWidgetCollection.Instance.add({
  name: "video",
  title: "Video",
  isFit: (question) => {
    return question.getType() === 'video';
  },
  activatedByChanged: (activatedBy) => {
    // @ts-ignore
    Survey.JsonObject.metaData.addClass("video", [], "", "empty");
  },
  htmlTemplate: `<div><video controls><source id="videoSource" type="video/mp4"></video></div>`,
  afterRender: (question, el) => {
    var videoElement = el.querySelector('video');
    var sourceElement = el.querySelector('#videoSource');
    sourceElement.src = question.videoUrl; // Make sure to provide the video URL in the question
    videoElement.load();
  }
});



// Add property for the video URL
Survey.JsonObject.metaData.addProperty('video', 'videoUrl');

const Inspection = () => {
  // const token = JSON.parse(sessionStorage.getItem("user-info")!).token;
  const [currentSurveyJson, setCurrentSurveyJson] = useState(initialSurveyJson);
  const navigate = useNavigate()
  
  const survey = new Model(currentSurveyJson);
  
  const BridgeRequest = (result: Model) => {
    console.log(result.data)
    let f: any = inspectionSurveyJson
    navigate(`/query-element-images?bridge=${result.data.bridge}&element=${result.data.element}`)
  }

  survey.onComplete.add(BridgeRequest);

  return (
    <main style={{ padding: "1rem 0" }}>
      <h2>Inspection</h2>
      <SurveyUI model={survey} />;
    </main>
  );
};

export default Inspection;
