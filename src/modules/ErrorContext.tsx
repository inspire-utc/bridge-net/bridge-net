import React, { createContext, useState, useContext } from 'react';

import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

const MessageBubble = ({ message }) => {
  return (
    <Paper elevation={3} style={{ padding: '10px 20px', margin: '10px', maxWidth: '300px', borderRadius: '20px', position:"absolute", backgroundColor:"#f44336"}}>
      <Typography variant="body1">{message}</Typography>
    </Paper>
  );
};


interface ErrorContextType {
    error: {
      message: string;
      isVisible: boolean;
    };
    showError: (message: string) => void;
    hideError: () => void;
}

const ErrorContext = createContext<ErrorContextType>({
  error: {
    message: '',
    isVisible: false,
  },
  showError: (message) => {},
  hideError: () => {}
});

export const useError = () => useContext(ErrorContext);

export const ErrorProvider = ({ children }) => {
  const [error, setError] = useState({ message: '', isVisible: false });

  const showError = (message, timeVisible=5) => {
    setError({ message, isVisible: true });

    setTimeout(hideError, timeVisible * 1000)
  };

  const hideError = () => {
    setError({ message: '', isVisible: false });
  };

  return (
    <ErrorContext.Provider value={{ error, showError, hideError }}>
      <>
        { error.isVisible &&  <MessageBubble message={error.message}/>}
      </>
      {children}
      
    </ErrorContext.Provider>
  );
};
