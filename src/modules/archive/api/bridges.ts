import axios from "axios";
import { API_ROOT_URL } from "../../../utils/vars";

//consult the api for details on potential parameters
interface BridgeRequestParameters {
  [key: string]: number;
}

export const getBridge = async (bridgeId: number) => {
  const { data } = await axios.get(`${API_ROOT_URL}bridges/${bridgeId}`);

  return data;
};

export const getBridges = async (bridgeParams?: BridgeRequestParameters) => {
  const { data } = await axios.get(`${API_ROOT_URL}bridges`, {
    params: bridgeParams,
  });

  return data;
};

export const postBridge = () => {};
