/**
 * Author: Timothy Headrick
 * Desc: This component renders a horizontal, scrollable list of a set amount of images. It is capped off by a link to more images
 * if the seeMoreRoute prop is provided.
 */
import { Link, createSearchParams } from "react-router-dom";
import "../../../styles/Archive/Gallery.css";
import { ImageGrid } from "./bridge-gallery/ImageGrid";
import axios from "axios";
import { useState, useEffect, useCallback } from "react"
import { Button } from "@mui/material"
import { useNavigate } from "react-router-dom";

export interface IImageData {
  file: string; //actual raw display page for the image
  url: string; //link to image object in api
}


export interface IGalleryBridgeProps {
  count: number; //How many images to render from the provided array of images
  seeMoreRoute?: string; //Link where more images can be viewed
  imageData: IImageData[] /*Array of images accepted. This method decreases how universally usable this component is but
   increases performance in our use case since the api is already providing all this data when bridges are fetched in Gallery.
   Ie less api calls overall.*/;
}



function getFileTypeFromSrc(src) {
  const extension = src.split('.').pop();
  return extension.split('?')[0];
}

function VideoComponent({ src, videoid }) {
  console.log(src)
  const navigate = useNavigate()
  const redirect = () => {
    const params = {
      videoid,
    }
    navigate({
      pathname: "/archive/videoquery",
      search: `?${createSearchParams(params)}`
    })
  }
  const fileType = getFileTypeFromSrc(src);
  return (
      <div>
          <video width="320" height="240" controls>
              <source src={src} type={`video/${fileType}`} />
              Your browser does not support the video tag.
          </video>
          <div>
            <Button onClick={redirect}>Query Video</Button>
          </div>
      </div>
  );
}

export default VideoComponent;

const extractNumberAfterVideos = (url) => {
  const parts = url.split('/videos/');
  return parts.length > 1 ? parts[1].split('/')[0] : null;
};

async function getVideoURL(imageData: any []) {
  let array: any[] = []
  for (let image of imageData) {
    let result = await axios.get(image)
    array.push({url: result.data.file, videoid: extractNumberAfterVideos(result.data.url) })
  }
  return array;
}

export const VideoGalleryBridge = (props: IGalleryBridgeProps) => {
  
  const [ getBridgeFiles, setBridgeFiles ] = useState<string []>([]);
  useEffect(() => {
    getVideoURL(props.imageData)
      .then(setBridgeFiles)
  }, [props.imageData,  props.imageData.length]); // dependencies
  
  return (
    <div className="Gallery-bridge-root">
      {/* Display "No Images Found" in identically sized box if no images where provided.*/}
      {props.imageData.length === 0 && (
        <div className="Gallery-bridge-remaining">
          <p>No Videos Found</p>
        </div>
      )}
      {/* Render ${count} number of images.*/}
      {
        getBridgeFiles.map((src: any) => <VideoComponent src={src.url} videoid={src.videoid}/>)
      }
      {/* Render "See More Images" box if there are still more images to display. */}
      {props.seeMoreRoute && props.imageData.length - props.count > 0 && (
        <Link to={props.seeMoreRoute}>
          <div className="Gallery-bridge-remaining">
            <p>See {props.imageData.length - props.count} more</p>
          </div>
        </Link>
      )}
    </div>
  );
};
