/**
 * Author: Timothy Headrick
 * Desc: Displays a Collapsable group of all images at a provided elementUrl or imagesUrl.
 * TODO: Should also display an icon indicating if a given image has a condition associated with it.
 */
import Collapse from "@mui/material/Collapse";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import axios from "axios";
import { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import { ImageGrid } from "./ImageGrid";

export interface ICollapsableImageGroupProps {
  elementUrl?: string;
  imagesUrl?: string;
  startClosed?: boolean;
}

export const CollapsableImageGroup = (props: ICollapsableImageGroupProps) => {
  const [data, setData] = useState({
    elementName: null,
    imageData: [{ url: "", file: "", condition: null }],
  });
  const [open, setOpen] = useState(props.startClosed ? false : true); //start open unless startClosed provided

  const handleClick = () => {
    setOpen(!open);
  };

  useEffect(() => {
    /* If an elementUrl was provided get data this way */
    if (props.elementUrl !== undefined) {
      axios.get(props.elementUrl).then((response) => {
        setData({
          elementName: response.data.name,
          imageData: response.data.images,
        });
      });
    } else if (props.imagesUrl) {
      /* If an imagesUrl was provided get data this way */
      axios.get(props.imagesUrl).then((response) => {
        setData({
          elementName: response.data.results.name,
          imageData: response.data.results.map((image: any) => ({
            url: image.url,
            file: image.file,
            condition: image.condition,
          })),
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className="Collapsable-image-group-header-parent">
        {/* Set name for group based on elementName or Miscellaneous if not element */}
        <h1>{data.elementName ? data.elementName : "Miscellaneous"}</h1>
        {/* MaterialUI button icon that changes based on open status */}
        <Button onClick={handleClick}>
          {!open ? <AddIcon /> : <RemoveIcon />}
        </Button>
      </div>
      {/* Collapsable group of images */}
      <Collapse
        in={open}
        timeout={250 /* Take 250ms to collapse and open */}
        unmountOnExit
      >
        <ImageGrid imageData={data.imageData} />
      </Collapse>
    </div>
  );
};
