/**
 * Author: Timothy Headrick
 * Desc: Given standard img props, it generates an image as usual but adds a small icon to the bottom right corner.
 * This is intended to be used to indicate that an image is associated with a condition.
 */

import DesignServicesIcon from "@mui/icons-material/DesignServices";

export const ConditionImageOverlay = (props: any) => {
  return (
    <div style={{ position: "relative", top: 0, left: 0 }}>
      <img {...props} alt="Bridge" />
      <DesignServicesIcon
        style={{
          color: "white",
          backgroundColor: "grey",
          borderRadius: "0.5rem",
          position: "absolute",
          bottom: "0.3rem",
          right: "0.7rem",
        }}
      />
    </div>
  );
};
