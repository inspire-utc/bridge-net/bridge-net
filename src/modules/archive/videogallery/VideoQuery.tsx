import React, { FC } from 'react';
import { useState, useCallback, useRef, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import axios from "axios"

import './VideoQuery.css';

import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';



const VideoQuery: FC = () => {
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const videoId = queryParams.get("videoid")
    const ref = useRef<HTMLVideoElement | null >(null)
    const [ videoIndex, setVideoIndex ] = useState<any | null>(null)
    const [ videoUrl, setVideoURL ] = useState("")
    
    const handleButtonClick = (frameSec) => {
        if (ref.current) {
            console.log(ref.current.seekable, ref.current.readyState)
            ref.current.currentTime = frameSec
            ref.current.play()
        }
    };

    
    useEffect(() => {
        axios.get(`http://localhost:8000/api/videos/?id=${videoId}`)
            .then((data) => {
                setVideoURL(data.data.results[0].file)
            })
        axios.get(`http://localhost:8000/api/video-index/?video_id=${videoId}`)
            .then((data) => {
                const result: any[] = data.data.results[0].index
                result.sort((a, b) => a.frame_no - b.frame_no)
                const elementset = new Set()
                result.forEach((index) => {
                  index.labels.forEach((label) => elementset.add(label))  
                })
                const elementlist = Array.from(elementset)
                
                const newDataStructure = result.map(item => {
                    const elements = elementlist.map(element => item.labels.includes(element));
                    return {
                        frame_sec: item.frame_sec,
                        elements: elements
                    };
                });
                console.log(newDataStructure)
                setVideoIndex({
                    elementlist,
                    index: newDataStructure
                })

            })
    }, [setVideoURL, setVideoIndex])
    
   
    useEffect(() => {
        if (ref.current) {
          ref.current.load();
        }
      }, [videoUrl]);

    useEffect(() => {

    }, [])

    console.log(videoIndex)
    return (
        <div className='app-container'>
            <Paper>
                <video ref={ref} width="800" controls preload="auto">
                    <source src={videoUrl} type="video/mp4" />
                    Your browser does not support the video tag.
                </video>
            </Paper>

            {   videoIndex &&
                <TableContainer component={Paper} className="table-container">
                <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell>Frame Sec</TableCell>
                            {videoIndex.elementlist.map((element, index) => (
                                <TableCell key={index}>{element}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {videoIndex.index.map((row, rowIndex) => (
                            <TableRow key={rowIndex}>
                                <TableCell>{row.frame_sec}</TableCell>
                                {row.elements.map((element, elementIndex) => (
                                    <TableCell key={elementIndex}> {element && (
                                        <Button variant="contained" onClick={() => handleButtonClick(row.frame_sec)}>
                                            Go to {row.frame_sec}s
                                        </Button>
                                    )}</TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                </TableContainer>
            }
        </div>
    );
};

export default VideoQuery;
