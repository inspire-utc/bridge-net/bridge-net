/**
 * Author: Timothy Headrick
 * Desc: Modal for uploading an element to the database. The user must be longed in to be successful.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "../../../../styles/LoginPopup.css";
import TextField from "@mui/material/TextField";

import FormGroup from "@mui/material/FormGroup";
// import Autocomplete from "@mui/material/Autocomplete";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import axios from "axios";
import { API_ROOT_URL } from "../../../../utils/vars";

export interface ICreateElementProps {
  on: boolean;
  handleOn: () => void;
}

export const CreateElementPopup = ({ on, handleOn }: ICreateElementProps) => {
  const [elementName, setElementName] = useState("");
  const [elementNumber, setElementNumber] = useState(0);
  const [bridge, setBridge] = useState(0);

  //anytime the username textInput changes
  const handleElementName: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setElementName(event.target.value);
  };

  //anytime the password input changes
  const handleElementNumber: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setElementNumber(event.target.value);
  };

  const handleBridge: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setBridge(event.target.value);
  };

  const handleCreateElement = () => {
    console.log(JSON.parse(sessionStorage.getItem("user-info")!).token);
    axios
      .post(
        `${API_ROOT_URL}elements/`,
        {
          name: elementName,
          element_number: elementNumber,
          bridge: `${API_ROOT_URL}bridges/${bridge}/`,
        },
        {
          headers: {
            Authorization: `Token ${
              JSON.parse(sessionStorage.getItem("user-info")!).token
            }`,
          },
        }
      )
      .then((response) => {
        console.log(
          "Successfully posted element: " + JSON.stringify(response.data)
        );

        handleOn();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      {on && (
        <div className="login-background">
          <div className="login-popup" style={{ height: "40rem" }}>
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
             Because of that tendency, there may be times where the style attribute needs
             to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <FormControl
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label className="login-label-header">Create New Element</label>
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                style={{ width: "20rem", height: "2rem", marginTop: "5%" }}
                color="primary"
                value={elementName}
                onChange={handleElementName}
                type="text"
                label="Element Name"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={elementNumber}
                onChange={handleElementNumber}
                type="number"
                label="Element Number"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={bridge}
                onChange={handleBridge}
                type="number"
                label="Bridge Structure Number"
                variant="standard"
              />
              <FormGroup row style={{ marginTop: "10%", width: "90%" }}>
                <Button
                  variant="contained"
                  style={{ marginLeft: "auto" }}
                  sx={{
                    backgroundColor: "#005F83",
                    "&:hover": {
                      backgroundColor: "#007A33",
                    },
                  }} //like style but support for more things related to MaterialUI
                  onClick={handleCreateElement}
                >
                  Submit
                </Button>
              </FormGroup>
            </FormControl>
          </div>
        </div>
      )}
    </>
  );
};
