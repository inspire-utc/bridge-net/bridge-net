/**
 * Author: Timothy Headrick
 * Desc: Modal for uploading a condition to the database. The user must be longed in to be successful.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "../../../../styles/LoginPopup.css";
import TextField from "@mui/material/TextField";

import FormGroup from "@mui/material/FormGroup";
//import Autocomplete from "@mui/material/Autocomplete";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import axios from "axios";
import { API_ROOT_URL } from "../../../../utils/vars";

export interface ICreateConditionProps {
  on: boolean;
  handleOn: () => void;
}

export const CreateConditionPopup = ({
  on,
  handleOn,
}: ICreateConditionProps) => {
  const [type, setType] = useState("");
  const [location, setLocation] = useState("");
  const [severity, setSeverity] = useState("");
  const [length, setLength] = useState("");
  const [units, setUnits] = useState("");
  const [comment, setComment] = useState("");
  const [element, setElement] = useState(0);

  const handleType: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setType(event.target.value);
  };

  const handleLocation: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setLocation(event.target.value);
  };

  const handleSeverity: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setSeverity(event.target.value);
  };

  const handleLength: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setLength(event.target.value);
  };

  const handleUnits: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setUnits(event.target.value);
  };

  const handleComment: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setComment(event.target.value);
  };

  const handleElement: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setElement(event.target.value);
  };

  const handleCreateCondition = () => {
    console.log(JSON.parse(sessionStorage.getItem("user-info")!).token);
    axios
      .post(
        `${API_ROOT_URL}conditions/`,
        {
          type: type,
          location: location,
          severity: severity,
          length: length,
          units: units,
          comment: comment,
          element: `${API_ROOT_URL}elements/${element}/`,
        },
        {
          headers: {
            Authorization: `Token ${
              JSON.parse(sessionStorage.getItem("user-info")!).token
            }`,
          },
        }
      )
      .then((response) => {
        console.log(
          "Successfully posted element: " + JSON.stringify(response.data)
        );

        handleOn();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      {on && (
        <div className="login-background">
          <div className="login-popup" style={{ height: "50rem" }}>
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
             Because of that tendency, there may be times where the style attribute needs
             to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <FormControl
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label className="login-label-header">Create New Condition</label>
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                style={{ width: "20rem", height: "1rem", marginTop: "5%" }}
                color="primary"
                value={type}
                onChange={handleType}
                type="text"
                label="Condition Type"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={location}
                onChange={handleLocation}
                type="number"
                label="Condition Location"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={element}
                onChange={handleSeverity}
                type="number"
                label="Condition Severity"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={element}
                onChange={handleLength}
                type="number"
                label="Condition Length"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={element}
                onChange={handleUnits}
                type="number"
                label="Length Units"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={element}
                onChange={handleComment}
                type="number"
                label="Condition Comment"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "1rem", marginTop: "7.5%" }}
                value={element}
                onChange={handleElement}
                type="number"
                label="Parent Element"
                variant="standard"
              />
              <FormGroup row style={{ marginTop: "10%", width: "90%" }}>
                <Button
                  variant="contained"
                  style={{ marginLeft: "auto" }}
                  sx={{
                    backgroundColor: "#005F83",
                    "&:hover": {
                      backgroundColor: "#007A33",
                    },
                  }} //like style but support for more things related to MaterialUI
                  onClick={handleCreateCondition}
                >
                  Submit
                </Button>
              </FormGroup>
            </FormControl>
          </div>
        </div>
      )}
    </>
  );
};
