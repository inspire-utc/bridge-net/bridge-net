/**
 * Author: Timothy Headrick
 * Desc: Modal for uploading an image to the database. The user must be longed in to be successful.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "../../../../styles/LoginPopup.css";

import FormGroup from "@mui/material/FormGroup";
import { Button, FormControl } from "@mui/material";
import axios from "axios";
import { API_ROOT_URL } from "../../../../utils/vars";
import { BridgeSearch } from "../selectors/BridgeSearch";
import { ElementSearch } from "../selectors/ElementSearch";
import { ConditionSearch } from "../selectors/ConditionSearch";

export interface ICreateImageProps {
  on: boolean;
  handleOn: () => void;
  isVideo: boolean
}

export const CreateImagePopup = ({ on, handleOn, isVideo }: ICreateImageProps) => {
  const [files, setFiles] = useState();
  const [bridge, setBridge] = useState("");
  const [element, setElement] = useState("");
  const [condition, setCondition] = useState("");

  //TODO: Need to create a autocomplete that only allows inputting ids that exist

  const uploadLabel = isVideo ? "Upload Video" : "Upload Image"
  const uploadMime = isVideo ? "video/*" : "image/*"
  const postLocation = isVideo ? `${API_ROOT_URL}videos/` : `${API_ROOT_URL}images/`

  const handleCreateImage = () => {
    var formData = new FormData();
    console.log(files);
    // console.log(files);
    // let arrayOfFiles: File[] = [];
    for (let i = 0; i < files!["length"]; i++) {
      formData.append("file", files![i]);
      console.log(files![i]);
    }
    // console.log(arrayOfFiles);
    // formData.append("files", arrayOfFiles!);
    if (bridge !== "")
      formData.append("bridge", `${API_ROOT_URL}bridges/${bridge}/`);
    if (element !== "")
      formData.append("element", `${API_ROOT_URL}elements/${element}/`);
    if (condition !== "")
      formData.append("condition", `${API_ROOT_URL}conditions/${condition}/`);
    axios
      .post(postLocation, formData, {
        headers: {
          Authorization: `Token ${
            JSON.parse(sessionStorage.getItem("user-info")!).token
          }`,
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        console.log(
          "Successfully posted bridge: " + JSON.stringify(response.data)
        );

        handleOn();
      })
      .catch(() => {
        console.log("Error while posting videos(s)");
      });
  };

  return (
    <>
      {on && (
        <div className="login-background">
          <div className="login-popup" style={{ height: "35rem" }}>
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
             Because of that tendency, there may be times where the style attribute needs
             to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <FormControl
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label className="login-label-header">{uploadLabel}</label>
              <Button
                variant="contained"
                component="label"
                sx={{
                  marginTop: "10%",
                  backgroundColor: "#005F83",
                  "&:hover": {
                    backgroundColor: "#007A33",
                  },
                }}
              >
                Upload
                <input
                  hidden
                  accept={uploadMime}
                  multiple
                  type="file"
                  onChange={(event: any) => {
                    setFiles(event.target.files);
                  }}
                />
              </Button>
              <BridgeSearch
                onChange={(value: string) => {
                  console.log(value)
                  setBridge(value);
                }}
              />
              <ElementSearch
                onChange={(value: string) => {
                  setElement(value);
                }}
              />
              <ConditionSearch
                onChange={(value: string) => {
                  setCondition(value);
                }}
              />
              <FormGroup row style={{ marginTop: "10%", width: "90%" }}>
                <Button
                  variant="contained"
                  style={{ marginLeft: "auto" }}
                  sx={{
                    backgroundColor: "#005F83",
                    "&:hover": {
                      backgroundColor: "#007A33",
                    },
                  }} //like style but support for more things related to MaterialUI
                  onClick={handleCreateImage}
                >
                  Submit
                </Button>
              </FormGroup>
            </FormControl>
          </div>
        </div>
      )}
    </>
  );
};
