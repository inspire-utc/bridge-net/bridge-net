/**
 * Author: Timothy Headrick
 * Desc: Modal for uploading a bridge to the database. The user must be longed in to be successful.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "../../../../styles/LoginPopup.css";
import TextField from "@mui/material/TextField";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import { Button, FormControl, FormControlLabel } from "@mui/material";
import axios from "axios";
import { API_ROOT_URL } from "../../../../utils/vars";

export interface ICreateBridgeProps {
  on: boolean;
  handleOn: () => void;
}

export const CreateBridgePopup = ({ on, handleOn }: ICreateBridgeProps) => {
  const [structureNumber, setStructureNumber] = useState(0);
  const [stateCode, setStateCode] = useState("");
  const [countyName, setCountyName] = useState("");
  const [yearBuilt, setYearBuilt] = useState(0);
  const [structureType, setStructureType] = useState<"SG" | "CG">("CG");
  const [isLTBP, setIsLTBP] = useState(false);

  //anytime the structure_number changes
  const handleStructureNumber: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setStructureNumber(event.target.value);
  };

  const handleStateCode: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setStateCode(event.target.value);
  };

  const handleCountyName: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event): void => {
    setCountyName(event.target.value);
  };

  const handleYearBuilt: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event): void => {
    setYearBuilt(parseInt(event.target.value));
  };

  const handleStructureTypeCG: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event): void => {
    setStructureType("CG");
  };

  const handleStructureTypeSG: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event): void => {
    setStructureType("SG");
  };

  const handleIsLTBP: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event: any): void => {
    setIsLTBP(event.target.checked);
  };

  const handleCreateBridge = () => {
    console.log(JSON.parse(sessionStorage.getItem("user-info")!).token);
    axios
      .post(
        `${API_ROOT_URL}bridges/`,
        {
          structure_number: structureNumber,
          state_code: stateCode,
          county_name: countyName,
          year_built: yearBuilt,
          is_ltbp: isLTBP,
          structure_type: structureType,
        },
        {
          headers: {
            Authorization: `Token ${
              JSON.parse(sessionStorage.getItem("user-info")!).token
            }`,
          },
        }
      )
      .then((response) => {
        console.log(
          "Successfully posted bridge: " + JSON.stringify(response.data)
        );

        handleOn();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      {on && (
        <div className="login-background">
          <div className="login-popup" style={{ height: "50rem" }}>
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
             Because of that tendency, there may be times where the style attribute needs
             to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <FormControl
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label className="login-label-header">Create New Bridge</label>
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                style={{ width: "20rem", height: "2rem", marginTop: "5%" }}
                color="primary"
                value={structureNumber}
                onChange={handleStructureNumber}
                type="number"
                label="Structure Number"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={stateCode}
                onChange={handleStateCode}
                type="text"
                label="State Code"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={countyName}
                onChange={handleCountyName}
                type="text"
                label="County Name"
                variant="standard"
              />
              <TextField
                className="login-input"
                InputProps={{ classes: { input: "login-input" } }}
                color="primary"
                style={{ width: "20rem", height: "2rem", marginTop: "10%" }}
                value={yearBuilt}
                onChange={handleYearBuilt}
                type="number"
                label="Year Built"
                variant="standard"
              />
              <FormGroup
                row
                style={{ marginTop: "5%", width: "90%", marginLeft: "15%" }}
              >
                <div>Structure Type:</div>
                <FormControlLabel
                  value="cg"
                  control={
                    <Checkbox
                      checked={structureType === "CG"}
                      onChange={handleStructureTypeCG}
                    />
                  }
                  label="pre-stress concrete girder"
                  labelPlacement="start"
                />
                <FormControlLabel
                  value="sg"
                  control={
                    <Checkbox
                      checked={structureType === "SG"}
                      onChange={handleStructureTypeSG}
                    />
                  }
                  label="steel girder"
                  labelPlacement="start"
                />
              </FormGroup>
              <FormControlLabel
                value="is-ltbp"
                control={<Checkbox checked={isLTBP} onChange={handleIsLTBP} />}
                label="Is LTBP (Long Term Period Bridge)"
                labelPlacement="start"
              />
              <FormGroup
                row
                style={{
                  marginTop: "5%",
                  alignContent: "center",
                  width: "90%",
                }}
              >
                <Button
                  variant="contained"
                  style={{ marginLeft: "auto" }}
                  sx={{
                    backgroundColor: "#005F83",
                    "&:hover": {
                      backgroundColor: "#007A33",
                    },
                  }} //like style but support for more things related to MaterialUI
                  onClick={handleCreateBridge}
                >
                  Submit
                </Button>
              </FormGroup>
            </FormControl>
          </div>
        </div>
      )}
    </>
  );
};
