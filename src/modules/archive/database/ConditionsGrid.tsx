/**
 * Author: Timothy Headrick
 * Desc: Table dedicated to Conditions in the database.
 */

import { useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react"; // the AG Grid React Component

import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-balham.css"; // Optional theme CSS
import "../../../styles/Archive/ag-theme-balham-ow.css"; //Our ag-grid theme customizations
import "../../../styles/Archive/Archive.css"; //Typical css
import { API_ROOT_URL } from "../../../utils/vars";
import {
  foreignKeyFilterParams,
  getDataSource,
  numberFilterParams,
  textFilterParams,
} from "./ag-grid-universal";
import { GridReadyEvent } from "ag-grid-community/dist/lib/events";
import { useSearchParams } from "react-router-dom";
import Fab from "@mui/material/Fab";
import { CreateConditionPopup } from "./create-objects/CreateCondition";
import AddIcon from "@mui/icons-material/Add";
import AnimatedGrid from "./AnimatedGrid";

const ConditionsGrid = () => {
  const datasourcePageSize = 100;
  const datasourceUrl = API_ROOT_URL + "conditions/";
  const dataSource = getDataSource(datasourceUrl, datasourcePageSize);
  const [searchParams] = useSearchParams();
  const [createConditionPopupOn, setCreateConditionPopupOn] = useState(false);

  // Each Column Definition results in one Column.
  const columnDefs = [
    {
      headerName: "Type",
      field: "type",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Location",
      field: "location",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Severity",
      field: "severity",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Length",
      field: "length",
      filter: "agNumberColumnFilter",
      filterParams: numberFilterParams,
    },
    {
      headerName: "Units",
      field: "units",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Comment",
      field: "comment",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Parent Element ID",
      field: "element",
      filter: "agTextColumnFilter",
      filterParams: foreignKeyFilterParams,
      //Show bridge structure_number instead of link
      valueFormatter: (params: any) => {
        if (params.value) {
          const splitLink = params.value.split("/");
          return splitLink[splitLink.length - 2];
        }
      },
    },
  ];

  // DefaultColDef sets props common to all Columns
  const defaultColDef = useMemo(
    () => ({
      resizable: true,
      sortable: true,
      flex: 1,
    }),
    []
  );

  //   const onCellDoubleClicked = (event: CellDoubleClickedEvent<any>) => {
  //     if (event.colDef.field === "element") {
  //       const splitLink = event.data.element;

  //       navigate("/archive/database/elements?element=" + splitLink);
  //     }
  //   };

  //checks if a filter needs to be pre-applied from search_params
  const onGridReady = (e: GridReadyEvent<any>) => {
    const element = searchParams.get("element");
    //if a bridge filter needs to be pre-applied, doso
    if (element) {
      e.api.setFilterModel({
        element: {
          filterType: "text",
          type: "equals",
          filter: element,
        },
      });
    }
  };

  return (
    <div className="Archive-database-root">
      <AnimatedGrid>
        <div className="ag-theme-balham Archive-grid-base">
          <AgGridReact
            columnDefs={columnDefs} // Column Defs for Columns
            defaultColDef={defaultColDef} // Default Column Properties
            animateRows={true} // Optional - set to 'true' to have rows animate when sorted
            rowSelection="multiple" // Options - allows click selection of rows
            rowModelType="infinite"
            cacheBlockSize={datasourcePageSize}
            cacheOverflowSize={2}
            maxConcurrentDatasourceRequests={1}
            infiniteInitialRowCount={1000}
            maxBlocksInCache={10}
            datasource={dataSource}
            onGridReady={onGridReady}
            //onCellDoubleClicked={onCellDoubleClicked}
          />
        </div>
      </AnimatedGrid>
      {sessionStorage.getItem("user-info") && (
        <Fab
          sx={{
            position: "fixed",
            bottom: "5%",
            right: "2.5%",
            backgroundColor: "#005F83",
            color: "white",
            "&:hover": {
              backgroundColor: "#007A33",
            },
          }}
          aria-label="add"
          onClick={() => setCreateConditionPopupOn(!createConditionPopupOn)}
        >
          <AddIcon />
        </Fab>
      )}
      <CreateConditionPopup
        on={createConditionPopupOn}
        handleOn={() => setCreateConditionPopupOn(!createConditionPopupOn)}
      />
    </div>
  );
};

export default ConditionsGrid;
