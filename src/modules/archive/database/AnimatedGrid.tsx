import { motion } from "framer-motion";

const animations = {
  initial: { opacity: 0, y: "100%" },
  animate: { opacity: 1, y: 0, height: "auto" },
  exit: { opacity: 0, height: 0 },
};

type Props = JSX.Element | JSX.Element[] | string | string[];

const AnimatedGrid = ({ children }: { children: Props }) => {
  return (
    <motion.div
      variants={animations}
      initial="initial"
      animate="animate"
      exit="exit"
      transition={{ duration: 0.5 }}
    >
      {children}
    </motion.div>
  );
};

export default AnimatedGrid;
