/**
 * Author: Timothy Headrick
 * Desc: Table dedicated to Elements in the database.
 */

import { useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react"; // the AG Grid React Component

import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-balham.css"; // Optional theme CSS
import "../../../styles/Archive/ag-theme-balham-ow.css"; //Our ag-grid theme customizations
import "../../../styles/Archive/Archive.css"; //Typical css
import { API_ROOT_URL } from "../../../utils/vars";
import {
  foreignKeyFilterParams,
  getDataSource,
  numberFilterParams,
  textFilterParams,
} from "./ag-grid-universal";
import {
  CellDoubleClickedEvent,
  GridReadyEvent,
} from "ag-grid-community/dist/lib/events";
import { useNavigate, useSearchParams } from "react-router-dom";
import Fab from "@mui/material/Fab";
import { CreateElementPopup } from "./create-objects/CreateElement";
import AddIcon from "@mui/icons-material/Add";
import AnimatedGrid from "./AnimatedGrid";

const ElementsGrid = () => {
  const datasourcePageSize = 100;
  const datasourceUrl = API_ROOT_URL + "elements/";
  const dataSource = getDataSource(datasourceUrl, datasourcePageSize);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const [createElementPopupOn, setCreateElementPopupOn] = useState(false);

  // Each Column Definition results in one Column.
  const columnDefs = [
    {
      headerName: "Name",
      field: "name",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Element Number",
      field: "element_number",
      filter: "agNumberColumnFilter",
      filterParams: numberFilterParams,
    },
    {
      headerName: "Parent Bridge Number",
      field: "bridge",
      filter: "agTextColumnFilter",
      filterParams: foreignKeyFilterParams,
      //Show bridge structure_number instead of link
      valueFormatter: (params: any) => {
        if (params.value) {
          const splitLink = params.value.split("/");
          return splitLink[splitLink.length - 2];
        }
      },
    },
    {
      headerName: "Parent Element",
      field: "parent_element",
      filter: "agTextColumnFilter",
      filterParams: foreignKeyFilterParams,
      //show parent element id instead of link
      valueFormatter: (params: any) => {
        if (params.value) {
          const splitLink = params.value.split("/");
          return splitLink[splitLink.length - 2];
        }
      },
    },
    {
      headerName: "Child Conditions",
      field: "conditions",
      sortable: false,
      valueFormatter: (params: any) => (params.value ? params.value.length : 0),
    },
    {
      headerName: "Images",
      field: "images",
      sortable: false,
      valueFormatter: (params: any) => (params.value ? params.value.length : 0),
    },
  ];

  // DefaultColDef sets props common to all Columns
  const defaultColDef = useMemo(
    () => ({
      resizable: true,
      sortable: true,
      flex: 1,
    }),
    []
  );

  //checks if a filter needs to be pre-applied from search_params
  const onGridReady = (e: GridReadyEvent<any>) => {
    const bridge = searchParams.get("bridge");
    //if a bridge filter needs to be pre-applied, doso
    if (bridge) {
      e.api.setFilterModel({
        bridge: {
          filterType: "text",
          type: "equals",
          filter: bridge,
        },
      });
    }

    // const element = searchParams.get("element");
    // //if a bridge filter needs to be pre-applied, doso
    // if (element) {
    //   e.api.setFilterModel({
    //     url: {
    //       filterType: "text",
    //       type: "equals",
    //       filter: element,
    //     },
    //   });
    // }
  };

  const onCellDoubleClicked = (event: CellDoubleClickedEvent<any>) => {
    if (event.colDef.field === "bridge") {
      const splitLink = event.data.bridge.split("/");

      navigate(
        "/archive/database/bridges?structure_number=" +
          splitLink[splitLink.length - 2]
      );
    }
    if (event.colDef.field === "images") {
      const splitLinkBridge = event.data.bridge.split("/");
      const splitLinkElement = event.data.url.split("/");
      navigate(
        "/archive/gallery/" +
          splitLinkBridge[splitLinkBridge.length - 2] +
          "?element=" +
          splitLinkElement[splitLinkElement.length - 2]
      );
    }
    if (event.colDef.field === "conditions") {
      const splitLinkElement = event.data.url.split("/");
      navigate(
        "/archive/database/conditions" +
          "?element=" +
          splitLinkElement[splitLinkElement.length - 2]
      );
    }
  };

  return (
    <div className="Archive-database-root">
      <AnimatedGrid>
        <div className="ag-theme-balham Archive-grid-base">
          <AgGridReact
            columnDefs={columnDefs} // Column Defs for Columns
            defaultColDef={defaultColDef} // Default Column Properties
            animateRows={true} // Optional - set to 'true' to have rows animate when sorted
            rowSelection="multiple" // Options - allows click selection of rows
            rowModelType="infinite"
            cacheBlockSize={datasourcePageSize}
            cacheOverflowSize={2}
            maxConcurrentDatasourceRequests={1}
            infiniteInitialRowCount={1000}
            maxBlocksInCache={10}
            datasource={dataSource}
            onGridReady={onGridReady}
            onCellDoubleClicked={onCellDoubleClicked}
          />
        </div>
      </AnimatedGrid>

      {sessionStorage.getItem("user-info") && (
        <Fab
          sx={{
            position: "fixed",
            bottom: "5%",
            right: "2.5%",
            backgroundColor: "#005F83",
            color: "white",
            "&:hover": {
              backgroundColor: "#007A33",
            },
          }}
          aria-label="add"
          onClick={() => setCreateElementPopupOn(!createElementPopupOn)}
        >
          <AddIcon />
        </Fab>
      )}
      <CreateElementPopup
        on={createElementPopupOn}
        handleOn={() => setCreateElementPopupOn(!createElementPopupOn)}
      />
    </div>
  );
};

export default ElementsGrid;
