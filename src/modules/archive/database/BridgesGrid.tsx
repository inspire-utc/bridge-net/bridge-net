/**
 * Author: Timothy Headrick
 * Desc: Table dedicated to Bridges in the database.
 */

import { useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react"; // the AG Grid React Component

import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-balham.css"; // Optional theme CSS
import "../../../styles/Archive/ag-theme-balham-ow.css"; //Our ag-grid theme customizations
import "../../../styles/Archive/Archive.css"; //Typical css
import { CellDoubleClickedEvent, GridReadyEvent } from "ag-grid-community";
import { API_ROOT_URL } from "../../../utils/vars";
import { useNavigate, useSearchParams } from "react-router-dom";
import {
  getDataSource,
  numberFilterParams,
  textFilterParams,
} from "./ag-grid-universal";
import { CreateBridgePopup } from "./create-objects/CreateBridge";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import AnimatedGrid from "./AnimatedGrid";

const BridgesGrid = () => {
  const datasourcePageSize = 100;
  const datasourceUrl = API_ROOT_URL + "bridges/";
  const dataSource = getDataSource(datasourceUrl, datasourcePageSize);
  const navigate = useNavigate(); // for routing
  const [searchParams] = useSearchParams();
  const [createBridgePopupOn, setCreateBridgePopupOn] = useState(false);

  // Each Column Definition results in one Column.
  const columnDefs = [
    {
      headerName: "Structure Number",
      field: "structure_number",
      filter: "agNumberColumnFilter",
      filterParams: numberFilterParams,
    },
    {
      headerName: "State Code",
      field: "state_code",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "State Number",
      field: "state_number",
      filter: "agNumberColumnFilter",
      filterParams: numberFilterParams,
    },
    {
      headerName: "County Name",
      field: "county_name",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Girder Type",
      field: "structure_type",
      filter: "agTextColumnFilter",
      filterParams: textFilterParams,
    },
    {
      headerName: "Year Built",
      field: "year_built",
      filter: "agNumberColumnFilter",
      filterParams: numberFilterParams,
    },
    {
      headerName: "Child Elements",
      field: "elements",
      sortable: false,
      //use the amount of child elements as the value in the column
      valueFormatter: (params: any) => (params.value ? params.value.length : 0),
    },
    {
      headerName: "Images",
      field: "images",
      sortable: false,
      //use the amount of images as the value in the column
      valueFormatter: (params: any) => (params.value ? params.value.length : 0),
    },
  ];

  // DefaultColDef sets props common to all Columns
  const defaultColDef = useMemo(
    () => ({
      resizable: true, //The user can resize columns as they want
      sortable: true, //All the columns are sortable by default
      flex: 1, //All the columns fill equal space by default (also helps the grid grow to window size)
    }),
    []
  );

  //checks if a filter needs to be pre-applied from search_params
  const onGridReady = (e: GridReadyEvent<any>) => {
    const structure_number = searchParams.get("structure_number");
    if (structure_number) {
      e.api.setFilterModel({
        structure_number: {
          filterType: "number",
          type: "equals",
          filter: structure_number,
        },
      });
    }
  };

  const onCellDoubleClicked = (event: CellDoubleClickedEvent<any>) => {
    if (event.colDef.field === "images")
      navigate("/archive/gallery/" + event.data.structure_number);
    else if (event.colDef.field === "elements")
      navigate(
        "/archive/database/elements?bridge=" + event.data.structure_number
      );
  };

  return (
    <div className="Archive-database-root">
      <AnimatedGrid>
        <div className="ag-theme-balham Archive-grid-base">
          <AgGridReact
            columnDefs={columnDefs} // Column Defs for Columns
            defaultColDef={defaultColDef} // Default Column Properties
            animateRows={true} // Optional - set to 'true' to have rows animate when sorted
            rowSelection="multiple" // Options - allows click selection of rows
            rowModelType="infinite"
            cacheBlockSize={datasourcePageSize}
            cacheOverflowSize={2}
            maxConcurrentDatasourceRequests={1}
            infiniteInitialRowCount={1000}
            maxBlocksInCache={10}
            datasource={dataSource}
            onCellDoubleClicked={onCellDoubleClicked}
            onGridReady={onGridReady}
          />
        </div>
      </AnimatedGrid>
      {sessionStorage.getItem("user-info") && (
        <Fab
          sx={{
            position: "fixed",
            bottom: "5%",
            right: "2.5%",
            backgroundColor: "#005F83",
            color: "white",
            "&:hover": {
              backgroundColor: "#007A33",
            },
          }}
          aria-label="add"
          onClick={() => setCreateBridgePopupOn(!createBridgePopupOn)}
        >
          <AddIcon />
        </Fab>
      )}
      <CreateBridgePopup
        on={createBridgePopupOn}
        handleOn={() => setCreateBridgePopupOn(!createBridgePopupOn)}
      />
    </div>
  );
};

export default BridgesGrid;
