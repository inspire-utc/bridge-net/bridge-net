/**
 * Author: Timothy Headrick
 * Desc: Root for database tab. Handles autonavigating to the bridges route and putting the sidebar in the correct position.
 * ERROR: Autonavigation messes up history, so the back error does not work on the bridges tab if auto-navigated.
 */
import { Navigate, Outlet, useLocation } from "react-router-dom";
import "../../../styles/Archive/Archive.css"; //Typical css
import GridSidebar from "./GridSidebar";

const Database = () => {
  let location = useLocation();
  if (
    location.pathname === "/archive/database" ||
    location.pathname === "/archive/database/"
  ) {
    return <Navigate to="bridges" />;
  }
  return (
    <div className="Archive-parent">
      <GridSidebar />
      <Outlet/>
    </div>
  );
};

export default Database;
