/**
 * Author: Timothy Headrick
 * Desc: All the ag-grid tables we use follow a similar scheme, so this file universallizes
 * everything between them that can be.
 */

import { IDatasource } from "ag-grid-community/dist/lib/interfaces/iDatasource";
import axios, { AxiosResponse } from "axios";

import "ag-grid-community/styles/ag-theme-balham.css"; // Optional theme CSS
import "../../../styles/Archive/ag-theme-balham-ow.css"; //Our ag-grid theme customizations
import "../../../styles/Archive/Archive.css"; //Typical css
import { getBridges } from "../api/bridges";

//what options to provide on text filters
export const textFilterParams: any = {
  filterOptions: ["equals", "contains", "startsWith", "endsWith"],
  suppressAndOrCondition: true,
};
//what options to provide on number filters
export const numberFilterParams: any = {
  filterOptions: [
    "equals",
    "lessThan",
    "lessThanOrEquals",
    "greaterThan",
    "greaterThanOrEquals",
    "inRange",
  ],
  suppressAndOrCondition: true,
};

//What options to provide on text filters of foreign keys
export const foreignKeyFilterParams: any = {
  filterOptions: ["equals"],
  suppressAndOrCondition: true,
};

//This adds the filter params to a given table. In other words, it
//translates filters provided by ag-grid into filters used by the
//api and applies those parameters to the request that will be sent.
const addFilterParams = (filterModel: any, requestParams: any): void => {
  const filters = Object.keys(filterModel);
  if (filters.length !== 0) {
    //for each filter applied
    filters.forEach((filter) => {
      //if it is a number filter
      if (filterModel[filter].filterType === "number") {
        switch (filterModel[filter].type) {
          case "equals":
            requestParams[filter] = filterModel[filter].filter;
            break;
          case "greaterThan":
            requestParams[`${filter}__gt`] = filterModel[filter].filter;
            break;
          case "greaterThanOrEquals":
            requestParams[`${filter}__gte`] = filterModel[filter].filter;
            break;
          case "lessThan":
            requestParams[`${filter}__lt`] = filterModel[filter].filter;
            break;
          case "lessThanOrEquals":
            requestParams[`${filter}__lte`] = filterModel[filter].filter;
            break;
          case "inRange":
            //inRange simply applies both a gte and lte for the upper and lower bound respectively
            requestParams[`${filter}__gte`] = filterModel[filter].filter;
            requestParams[`${filter}__lte`] = filterModel[filter].filterTo;
            break;
          default:
            break;
        }
      } // if it is a text filter
      else if (filterModel[filter].filterType === "text") {
        switch (filterModel[filter].type) {
          case "equals":
            requestParams[filter] = filterModel[filter].filter;
            break;
          case "contains":
            requestParams[`${filter}__icontains`] = filterModel[filter].filter;
            break;
          case "startsWith":
            requestParams[`${filter}__startswith`] = filterModel[filter].filter;
            break;
          case "endsWith":
            requestParams[`${filter}__endswith`] = filterModel[filter].filter;
            break;
          default:
            break;
        }
      }
    });
  }
};

//This adds the sort params to a given table. In other words, it
//translates the sort object provided by ag-grid into a sort parameter
//for the api request.
const addSortParams = (sortModel: any, requestParams: any): void => {
  if (sortModel[0]) {
    //ag-grid provides and "asc" or "desc" sort, the api uses ordering=field or -field
    //for ascending and descending ordering respectively
    requestParams["ordering"] =
      (sortModel[0].sort === "asc" ? "" : "-") + sortModel[0].colId;
  }
};

//The source object used by all the ag-grids.
//Only the url and pageSize needs to be changed for each grid.
export const getDataSource = (
  dataSourceUrl: string,
  dataSourcePageSize: number
): IDatasource => ({
  rowCount: undefined, //makes this infiniteRows for continuous requests
  getRows: (params: any) => {
    let page = params.endRow / dataSourcePageSize; //get the current api page
    let rowsThisPage: any;
    //when lastRow is not -1 it tells the grid to stop fetching pages.
    let lastRow: number = -1;
    //Used to store all request parameters. ie page, sorting, filters
    const requestParams: any = {
      page: page,
    };
    //Handles any sorting and filtering applied
    //These are applied continuously as they are entered, which is less than ideal.
    addSortParams(params.sortModel, requestParams);
    addFilterParams(params.filterModel, requestParams);

    try {
      getBridges()
    } catch(err) {

    }
    //The actual request for the data
    axios
      .get(dataSourceUrl, {
        params: requestParams,
      })
      .then((response: AxiosResponse) => {
        rowsThisPage = response.data.results;
        //If there is not another page
        if (response.data.next === null) {
          lastRow = (page - 1) * dataSourcePageSize + response.data.count;
        }
        params.successCallback(rowsThisPage, lastRow);
      })
      .catch(() => {
        //This catch can be triggered quite frequently when applying filters to foreign keys.
        //(A foreign key filter needs the provided id to exist or it gives a 400 error. )
        console.log("An invalid request was sent... recovering.");
        params.successCallback();
      });
  },
});

//Attempted to centralize grid creation to one component, but could not get it to work

// export const CustomAgGrid = (props: any) => {
//   const dataSourcePageSize = props.dataSourcePageSize;
//   const dataSourceUrl = props.dataSourceUrl;
//   const dataSource = getDataSource(dataSourceUrl, dataSourcePageSize);
//   const navigate = useNavigate(); // for routing

//   // DefaultColDef sets props common to all Columns
//   const defaultColDef = useMemo(
//     () => ({
//       resizable: true,
//       sortable: true,
//       flex: 1,
//     }),
//     []
//   );

//   const onCellDoubleClicked = (event: CellDoubleClickedEvent<any>) =>
//     navigate("/archive/gallery/" + event.data.structure_number);

//   return (
//     <div className="ag-theme-balham Archive-grid-base">
//       <AgGridReact
//         {...props}
//         defaultColDef={defaultColDef} // Default Column Properties
//         animateRows={true} // Optional - set to 'true' to have rows animate when sorted
//         rowSelection="multiple" // Options - allows click selection of rows
//         rowModelType="infinite"
//         cacheBlockSize={dataSourcePageSize}
//         cacheOverflowSize={2}
//         maxConcurrentDatasourceRequests={1}
//         infiniteInitialRowCount={1000}
//         maxBlocksInCache={10}
//         datasource={dataSource}
//       />
//     </div>
//   );
// };
