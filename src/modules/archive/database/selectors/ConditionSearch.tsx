import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import CircularProgress from "@mui/material/CircularProgress";
import { useState, useEffect, Fragment } from "react";
import axios, { AxiosResponse } from "axios";
import { API_ROOT_URL } from "../../../../utils/vars";

export const ConditionSearch = (props: {
  onChange: (arg0: string) => void;
}) => {
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState<number[]>([]);
  const [error, setError] = useState(false);
  const loading = open && !error && options.length === 0;

  const handleChange: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event) => {
    if (!isNaN(parseInt(event.target.value))) {
      setError(false);
      props.onChange(event.target.value);
      (async () => {
        axios
          .get(`${API_ROOT_URL}conditions/?id__gte=${event.target.value}`)
          .then((response: AxiosResponse) => {
            setOptions(
              response.data.results.map((condition: any) => condition.id)
            );
          });
      })();
    } else if (event.target.value.length > 0) {
      setError(true);
    }
  };

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  return (
    <Autocomplete
      id="asynchronous-demo"
      style={{ width: "20rem", height: "2rem", marginTop: "5%" }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      isOptionEqualToValue={(option, value) => option === value}
      getOptionLabel={(option) => `${option}`}
      options={options}
      filterOptions={(x) => x}
      loading={loading}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Condition Id"
          onChange={handleChange}
          className="login-input"
          color="primary"
          error={error}
          helperText={error ? "Make sure to only enter digits" : ""}
          variant="standard"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </Fragment>
            ),
          }}
        />
      )}
    />
  );
};
