/**
 * Author: Timothy Headrick
 * Desc: Sidebar to navigate between the different database tables.
 */
import { Link } from "react-router-dom";
import "../../../styles/Archive/Archive.css"; //Typical css

const GridSidebar = () => {
  return (
    <div className="Archive-grid-sidebar">
      <Link to="bridges">
        <div className="Archive-grid-sidebar-element">
          <p>Bridges</p>
        </div>
      </Link>
      <Link to="elements">
        <div className="Archive-grid-sidebar-element">
          <p>Elements</p>
        </div>
      </Link>
      <Link to="conditions">
        <div className="Archive-grid-sidebar-element">
          <p>Conditions</p>
        </div>
      </Link>
    </div>
  );
};

export default GridSidebar;
