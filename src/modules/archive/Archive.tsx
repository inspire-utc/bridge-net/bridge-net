/**
 * Author: Timothy Headrick
 * Desc: Root container for Archive page. Establishes {Database, Gallery} tabs and outlets to other pages.
 */
import { Outlet } from "react-router-dom";
import "../../styles/Archive/Archive.css"; //Typical css
import { MyTabs } from "./MyTabs";

const Archive = () => {
  return (
    <div>
      <div className="Archive-nav">
        <MyTabs />
      </div>
      <Outlet />
    </div>
  );
};

export default Archive;
