/**
 * Author: Timothy Headrick
 * Desc: Displays a grid of images based on a provided array of imageData.
 */
import { ConditionImageOverlay } from "../ConditionImageOverlay";

export interface IImageGridProps {
  imageData: { url: string; file: string; condition: null }[];
}

export const ImageGrid = (props: IImageGridProps) => {
  return (
    <div className="Collapsable-image-group-container">
      {props.imageData.length === 0 && (
        <div className="Horizontal-image-scroll-remaining">
          <p>No Images Found</p>
        </div>
      )}
      {/* Map image to individual img components */}
      {props.imageData.map((image: any) => {
        //if the image has a condition associated with it use the overlay
        if (image.condition) {
          return (
            <ConditionImageOverlay
              key={image.url}
              className="Collapsable-image-group-empty-image"
              src={image.file}
              alt={image.file}
            />
          );
        } else {
          return (
            <img
              key={image.url}
              className="Collapsable-image-group-empty-image"
              src={image.file}
              alt={image.file}
            />
          );
        }
      })}
    </div>
  );
};
