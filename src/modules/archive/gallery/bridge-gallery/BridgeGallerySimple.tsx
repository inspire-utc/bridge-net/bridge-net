/**
 * Author: Timothy Headrick
 * Desc: This compoenent is a simplified version of BridgeGallery. It is a gallery of images dedicated to a specific bridge. All
 *   it does is display all images for that bridge using the ImageGrid, it does not do any separation.
 *
 * Dynamic route: This uses a dynamic "structure_number" to know which bridge the route is associated with
 */

import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { API_ROOT_URL } from "../../../../utils/vars";
import { ImageGrid } from "./ImageGrid";

export const BridgeGallerySimple = () => {
  const { structure_number } = useParams(); //gets the dynicam route structure_number
  const bridgeUrl = API_ROOT_URL + "bridges/" + structure_number;
  const [imageData, setImageData] = useState([
    { url: "", file: "", condition: null },
  ]);

  useEffect(() => {
    /* If an elementUrl was provided get data this way */
    axios.get(bridgeUrl).then((response: any) => {
      setImageData(
        response.data.images.map((image: any) => ({
          file: image.file,
          url: image.url,
        }))
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <h1 className="Bridge-gallery-header">
        --- Images For MO - {structure_number} ---
      </h1>
      <ImageGrid imageData={imageData}></ImageGrid>
    </div>
  );
};
