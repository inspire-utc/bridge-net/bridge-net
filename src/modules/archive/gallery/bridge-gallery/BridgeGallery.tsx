/**
 * Author: Timothy Headrick
 * Desc: This component is a gallery of images dedicated to a specific bridge. It contains tabs for all the elements of that bridge
 * with their associated images contained with them in a CollapsableImageGroup. It also contains a final CollapsableImage Group
 * for and images that do not belong to an element.
 *
 * Dynamic route: This uses a dynamic "structure_number" to know which bridge the route is associated with
 * SearchParams: This has an optional "element" (id) search parameter that will tell all CollapsableImageGroups other
 *      then that element to start closed by default.
 */

import axios, { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import { API_ROOT_URL } from "../../../../utils/vars";
import { CollapsableImageGroup } from "./CollapsableImageGroup";

export const BridgeGallery = (props: any) => {
  const { structure_number } = useParams(); //gets the dynicam route structure_number
  const searchParams = useSearchParams()[0]; //setSearchParams is not needed here
  const [elementData, setElementData] = useState([]); //expects a list of element urls
  const bridgeUrl = API_ROOT_URL + "bridges/" + structure_number;
  const [elementParam, setElementParam] = useState<string | null>(null); //to store the potentially provided searchParam

  useEffect(() => {
    //Do the fetch for the elementData
    axios.get(bridgeUrl).then((response: AxiosResponse) => {
      setElementData(
        response.data.elements.map((element: string) => {
          const splitUrl = element.split("/");
          return {
            elementUrl: element,
            elementId: splitUrl[splitUrl.length - 2], //gets the id from the elementUrl,
          };
        })
      );
      setElementParam(searchParams.get("element")); //g this will be null if the elementParam was not provided
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <h1 className="Bridge-gallery-header">
        --- Images For MO - {structure_number} ---
      </h1>
      {/* Render all the CollapsableImageGroups based on specific elemnts, closing by default if needed */}
      {elementData.map(({ elementUrl, elementId }) => {
        if (elementParam === null || elementParam === elementId)
          return (
            <CollapsableImageGroup key={elementId} elementUrl={elementUrl} />
          );
        else if (elementParam !== elementId)
          return (
            <CollapsableImageGroup
              key={elementId}
              elementUrl={elementUrl}
              startClosed
            />
          );
        else return <></>;
      })}

      {/* Render the Miscellaneous CollapsableImageGroup, closing by default if needed. */}
      {elementParam === null && (
        <CollapsableImageGroup
          imagesUrl={`http://localhost:8000/api/images/?bridge=${structure_number}&element__isnull=true`}
        />
      )}
      {elementParam !== null && (
        <CollapsableImageGroup
          imagesUrl={`http://localhost:8000/api/images/?bridge=${structure_number}&element__isnull=true`}
          startClosed
        />
      )}
    </div>
  );
};
