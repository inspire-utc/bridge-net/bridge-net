/**
 * Author: Timothy Headrick
 * Desc: This component renders a horizontal, scrollable list of a set amount of images. It is capped off by a link to more images
 * if the seeMoreRoute prop is provided.
 */
import { Link } from "react-router-dom";
import "../../../styles/Archive/Gallery.css";

export interface IImageData {
  file: string; //actual raw display page for the image
  url: string; //link to image object in api
}

export interface IGalleryBridgeProps {
  count: number; //How many images to render from the provided array of images
  seeMoreRoute?: string; //Link where more images can be viewed
  imageData: IImageData[] /*Array of images accepted. This method decreases how universally usable this component is but
   increases performance in our use case since the api is already providing all this data when bridges are fetched in Gallery.
   Ie less api calls overall.*/;
}

export const GalleryBridge = (props: IGalleryBridgeProps) => {
  return (
    <div className="Gallery-bridge-root">
      {/* Display "No Images Found" in identically sized box if no images where provided.*/}
      {props.imageData.length === 0 && (
        <div className="Gallery-bridge-remaining">
          <p>No Images Found</p>
        </div>
      )}
      {/* Render ${count} number of images.*/}
      {props.imageData.slice(0, props.count).map((image: any) => (
        <img
          key={image.url}
          className="Gallery-bridge-image"
          src={image.file}
          alt={image.file}
        />
      ))}
      {/* Render "See More Images" box if there are still more images to display. */}
      {props.seeMoreRoute && props.imageData.length - props.count > 0 && (
        <Link to={props.seeMoreRoute}>
          <div className="Gallery-bridge-remaining">
            <p>See {props.imageData.length - props.count} more</p>
          </div>
        </Link>
      )}
    </div>
  );
};
