/**
 * Auther: Timothy Headrick
 * Desc: Root of Gallery tab. Displays a HorizontalImageScroll component for each bridge in the database.
 */

import Fab from "@mui/material/Fab";
import { useState } from "react";
import "../../../styles/Archive/Gallery.css"; //Typical css
import { API_ROOT_URL } from "../../../utils/vars";
import { GalleryBridge } from "./GalleryBridge";
import AddIcon from "@mui/icons-material/Add";
import { CreateImagePopup } from "../database/create-objects/CreateImage";
import { useQuery } from "react-query";
import { getBridges } from "../api/bridges";

const Gallery = () => {
  const bridgesUrl = API_ROOT_URL + "bridges";
  const [bridgeData, setBridgeData] = useState({
    bridgeData: [
      {
        state_code: "",
        structure_number: 0,
        images: [
          {
            url: "",
            file: "",
            images: [],
          },
        ],
      },
    ],
  }); //This defines the structure for useState while simultaneuosly providing a fallback in case things break.
  const [createImagePopupOn, setCreateImagePopupOn] = useState(false);

  const { data, error, isError, isLoading, isSuccess } = useQuery(
    ["bridges"],
    () => getBridges()
  );

  if (isError) {
    return <div></div>;
  }

  if (isLoading) {
    return <div></div>;
  }

  // if (isSuccess) {
  //   setBridgeData({
  //     bridgeData: data.results.map((bridge: any) => ({
  //       state_code: bridge.state_code,
  //       structure_number: bridge.structure_number,
  //       images: bridge.images,
  //     })),
  //   });
  // }

  // //useEffect with an empty [] as its second argument only runs once, which is typically ideal.
  // //This fetches all the bridges and keeps their state_code, structure_number, and images when the component is loaded.
  // useEffect(() => {
  //   axios.get(bridgesUrl).then((response: AxiosResponse) => {
  //     setBridgeData({
  //       bridgeData: response.data.results.map((bridge: any) => ({
  //         state_code: bridge.state_code,
  //         structure_number: bridge.structure_number,
  //         images: bridge.images,
  //       })),
  //     });
  //   });
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  //For each fetched bridge, create a header with its state_code and structure_number followed by the horizontal image scroll
  //component. The Horizontal image scroll component takes the images of the fetched bridge and is told to only render 10 images.
  //It is also passed the bridge's structure number to create a seeMoreRoute.
  return (
    <div>
      {data.results.map((bridge: any) => (
        <div key={bridge.structure_number}>
          <h1 className="Gallery-gallery-bridge-header">
            {bridge.state_code} - {bridge.structure_number}
          </h1>
          <GalleryBridge
            imageData={bridge.images}
            count={10}
            seeMoreRoute={`${bridge.structure_number}/min`}
          />
          {/* opens simple gallery view rather than element separated view */}
        </div>
      ))}
      {sessionStorage.getItem("user-info") && (
        <Fab
          sx={{
            position: "fixed",
            bottom: "5%",
            right: "5%",
            backgroundColor: "#005F83",
            color: "white",
            "&:hover": {
              backgroundColor: "#007A33",
            },
          }}
          aria-label="add"
          onClick={() => setCreateImagePopupOn(!createImagePopupOn)}
        >
          <AddIcon />
        </Fab>
      )}
      <CreateImagePopup
        on={createImagePopupOn}
        handleOn={() => setCreateImagePopupOn(!createImagePopupOn)}
        isVideo={false}
      />
    </div>
  );
};

export default Gallery;
