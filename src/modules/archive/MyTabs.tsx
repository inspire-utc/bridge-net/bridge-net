/**
 * Author: Timothy Headrick
 * Desc: Custom implementation of MaterialUI Tabs.
 */
import Tab from "@mui/material/Tab/Tab";
import Tabs from "@mui/material/Tabs/Tabs";
import { Link, useLocation } from "react-router-dom";

export const MyTabs = () => {
  const location = useLocation(); //for dynamically changing what tab is selected

  return (
    <Tabs
      value={location.pathname
        .split("/", 3)
        .join("/")
        .slice(1)} /* Change tab based on url path */
    >
      <Tab
        label="Database"
        component={Link}
        to="database"
        value="archive/database"
      />
      <Tab
        label="Image Gallery"
        component={Link}
        to="gallery"
        value="archive/gallery"
      />
      <Tab
        label="Video Gallery"
        component={Link}
        to="videogallery"
        value="archive/videogallery"
      />
    </Tabs>
  );
};
