/**
 * Author: Kevin Lai
 * Descr: Displays the tools needed to create projects and submit training jobs
 */
import { useLocation } from "react-router";
import { Outlet, Link } from "react-router-dom";
import Tab from "@mui/material/Tab/Tab";
import Tabs from "@mui/material/Tabs/Tabs";

const DNNTraining = () => {
  const location = useLocation();
  const current = location.pathname.split("/", 3).join("/").slice(1);
  console.log(current, location);
  return (
    <main style={{ padding: "1rem 0" }}>
      <h2>Deep Neural Network Training</h2>
      <div>
        <Tabs value={location.pathname.slice(1)}>
          <Tab
            label="Annotate"
            component={Link}
            to="annotations"
            value="dnn-training/annotator"
          />
          <Tab
            label="Workflows"
            component={Link}
            to="workflows"
            value="dnn-training/workflows"
          />
          <Tab 
            label="Monitor"
            component={Link}
            to="monitor"
            value="dnn-training/monitor"
          />
        </Tabs>
      </div>
      <Outlet />
    </main>
    
  );
};

export default DNNTraining;
