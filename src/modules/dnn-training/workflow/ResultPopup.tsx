/**
 * Author: Timothy Headrick
 * Desc: This file defines everything related to the login popup where users can enter their credentials.
 * It relies heavily on materialUI, so its styles can be slightly confusing. While it has css, there
 * are many things that require using the style prop.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "./ResultPopup.css"
import { post } from "../../../utils/requests";
import { TokenRounded } from "@mui/icons-material";


export interface ITriggerPopupProps {
  on: boolean;
  handleOn: () => void;
  selected_node: any;
  setNode: (node: any) => void;
}

export const ResultPopup = ({ on, handleOn, selected_node, setNode }: ITriggerPopupProps) => {
  console.log(selected_node)

  const workitem_id = selected_node.workitem_id
  
  let url = `http://localhost:8000/render/workitems/${workitem_id}`

  
  let user_info = JSON.parse(sessionStorage.getItem("user-info")!);
  let token = user_info ? "token=" + user_info.token : "";

  if (selected_node.data && selected_node.data.label === "Mask RCNN Inference" && token.length) {
    url += "?" + token
  }
  console.log(url)


  return (
    <>
      {on && (
        <div className="login-background">
          <div className="result-popup">
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
            Because of that tendency, there may be times where the style attribute needs
            to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <iframe 
              style={{ width: "100%", height: "700px" }}
              src={url}>
            </iframe>
          </div>
        </div>
      )}
    </>
  );
};
