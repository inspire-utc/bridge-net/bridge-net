import React, { useCallback, useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import constants from "./constants"
import TextField from '@mui/material/TextField';

import Graph from "./graph/graph"
 // @ts-ignore
import ReactFlow, { 
  useNodesState, 
  useEdgesState,
  applyNodeChanges, 
  applyEdgeChanges,
  addEdge,

} from 'reactflow';

import axios from "axios"
import dagre from "dagre";
import { Button } from "@mui/material";
import WorkItemNode from "./workitem/workitem"
import 'reactflow/dist/style.css';
import "./workflow.css"
import { TriggerPopup } from "./TriggerPopup"
import { post } from "../../../utils/requests";

import { useError } from "../../ErrorContext";

function Guid() {
  return crypto.randomUUID()
}



function findStartNodes(edges: any[], nodes: any[]) {
  const start_edges = edges.find((obj) => obj.source === '1')
  if (! start_edges)
    return [];
  const targets = edges.map((obj) => obj.target)
  return nodes.filter((obj) => targets.includes(obj.id))
}

const initialNodes = [
  { id: '1', position: { x: 250, y: 50 }, data: { label: 'Start' } },
];
const initialEdges = [];

function Workflow() {
    /* Dynamic routing - this way we can render a workflow based on the configuration stored in a database */
    const { guid } = useParams();

    const { showError } = useError();


    /* For setting and changing Workflow Name */
    const [ workflowName, setWorkflowName ] = useState("");
    const handleChangeWorkflowName = (event) => setWorkflowName(event.target.value)

    /* TriggerActive controls whether the "Activate Workflow" modal is active */
    const [ TriggerActive, setTriggerActive ] = useState(false);
    const toggleTriggerActive = () => { setTriggerActive(!TriggerActive) }

    /* dagreGraph is used purely for automatic layoutin */
    const dagreGraph = new dagre.graphlib.Graph()
    dagreGraph.setDefaultEdgeLabel(() => ({}));

    const nodeWidth = 172;
    const nodeHeight = 36;

    /* Store the state of the nodes. This is used by ReactFlow */
    const [nodes, setNodes, ] = useNodesState(initialNodes);
    const [edges, setEdges, ] = useEdgesState(initialEdges);

  
    useEffect(() => {
      const fetchData = async () => {
          try {
              const response = await axios.get(`http://localhost:8000/api/workflows/${guid}`);
              const { nodes, edges, name } = response.data;
              setNodes(nodes);
              setEdges(edges);
              setWorkflowName(name);
          } catch (error) {
              console.error('Error fetching data:', error);
          }
      };
      
      fetchData();
    }, []);  // The effect will rerun if `guid` changes

    /* Store the state of start nodes. We need this to dynamically generate the form used by surveyjs */
    const [ startNodes, setStartNodes ] = useState<any[]>([]);

    /* Set the state of the start nodes */
    useEffect(() => {
      const start_nodes = findStartNodes(edges, nodes)
      setStartNodes(start_nodes)
    }, [ edges  ])

    const [ nodeDeleteDiabled, setNodeDeleteDisabled ] = useState(true);
    const [ nodeSelected, setNodeSelected ] = useState({ id: null });

    const [ edgeDeleteDiabled, setEdgeDeleteDisabled ] = useState(true);
    const [ edgeSelected, setEdgeSelected ] = useState({ id: null });


    const onNodesChange = useCallback(
      (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
      []
    );
    const onEdgesChange = useCallback(
      (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
      []
    );
    const onConnect = (params) => {
      setEdges([
        ...edges,
        { id: Guid(), ...params }
      ])
    };

    const getLayoutedElements = (nodes, edges, direction = 'TB') => {
      const isHorizontal = direction === 'LR';
      dagreGraph.setGraph({ rankdir: direction });
    
      nodes.forEach((node) => {
        dagreGraph.setNode(node.id, { width: nodeWidth, height: nodeHeight });
      });
    
      edges.forEach((edge) => {
        dagreGraph.setEdge(edge.source, edge.target);
      });
    
      dagre.layout(dagreGraph);
    
      nodes.forEach((node) => {
        const nodeWithPosition = dagreGraph.node(node.id);
        node.targetPosition = isHorizontal ? 'left' : 'top';
        node.sourcePosition = isHorizontal ? 'right' : 'bottom';
    
        // We are shifting the dagre node position (anchor=center center) to the top left
        // so it matches the React Flow node anchor point (top left).
        node.position = 
        {
          x: nodeWithPosition.x - nodeWidth / 2,
          y: nodeWithPosition.y - nodeHeight / 2,
        };
    
        return node;
      });
    
      return { nodes, edges };
    };

    const setVerticalLayout = () => {
      const {nodes: lnodes, edges: ledges} = getLayoutedElements(nodes, edges, "TB");
      setNodes([...lnodes]);
      setEdges([...ledges]);
    }

    const setHorizontalLayout = () => {
      const {nodes: lnodes, edges: ledges} = getLayoutedElements(nodes, edges, "LR");
      setNodes([...lnodes]);
      setEdges([...ledges]);
    }

    const addSplitVideoNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 0, y: 0 }, data: { label: constants.SPLIT_VIDEO } } ])
    }

    const addFrameDiffNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 50, y: 50 }, data: { label: 'Frame Diff' } } ])
    }

    const addActiveLabelPropNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 100, y: 100 }, data: { label: '  ' } } ])
    }

    const addMaskRCNNInferenceNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 150, y: 150 }, data: { label: constants.MASK_RCNN_INFERENCE } } ])
    }

    const addAnnotationNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 200, y: 200 }, data: { label: 'User Annotation' } } ])
    }

    const addInspectioNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 250, y: 250 }, data: { label: 'User Inspection' } } ])
    }
    const addTrainingNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 300, y: 300 }, data: { label: 'Train Model' } } ])
    }

    const addDefisheyeNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 350, y: 350 }, data: { label: 'Defisheye' } } ])
    }

    const addMergeFramesNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 400, y: 400 }, data: { label: 'Merge Frames' } } ])
    }


    const addMockWorker = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 400, y: 400 }, data: { label: 'Mock Worker' } } ])
    }
    
    const addIndexVideoNode = () => {
      setNodes([...nodes, { id: Guid(), position: { x: 450, y: 450 }, data: { label: 'Index Video' } } ])

    }
    const deleteNode = () => {
      let newNodes = nodes.filter((item => item.id !== nodeSelected.id))
      setNodes(newNodes)
      setNodeDeleteDisabled(true)
    }
    
    const deleteEdge = () => {
      let newEdges = edges.filter((item) => item.id !== edgeSelected.id)
      setEdges(newEdges)
      setEdgeDeleteDisabled(true)
    }

    const onEdgeClick = (event, element) => {
      setEdgeDeleteDisabled(false)
      setEdgeSelected(element)
    }

    const onNodeClick = (event, element) => {
      setNodeDeleteDisabled(false)
      setNodeSelected(element)
    }
    
    const onPaneClick = () => {
      setNodeDeleteDisabled(true)
      setNodeSelected({ id: null })

      setEdgeDeleteDisabled(true)
      setEdgeSelected({ id: null })
    }


    const onSave = (name, guid: string | null = null) => {
      let user_info = JSON.parse(sessionStorage.getItem("user-info")!);
      let token = user_info.token;
      
      let config = {
        headers: {
          Authorization: "Token " + token
        }
      }

      const data = {
        id: guid ? guid : Guid(),
        nodes,
        edges,
        name,
      }
      
      let method;

      if (guid) {
        method = axios.put
      }
      else {
        method = axios.post
      }

      method('http://localhost:8000/api/workflows/', data, config)
        .then(response => {
          console.log('Success:', response);
          showError("Saved Successfully");
        })
        .catch(error => {
          console.error('Error:', error);
          showError("Failed to Save Workflow");
        });
    }

    const onTriggerWorkflow = () => {
      
      post("http://localhost:8000/api/workflowjobs/", {
        associated_workflow: guid,
      })
        .then(() => {
          console.log("TRIGGER WORKFLOW SUCCESS")
        })
        .catch(() => {
          console.log("TRIGGER WORKFLOW FAILURE")
        })
    }
    


    return <div>
      <div id="Ribbon">
        <div className="vertical-separator">
          <TextField label="Name" value={workflowName} onChange={handleChangeWorkflowName}/>
          <Button onClick={() => onSave(workflowName, guid)}>Save</Button>
          <Button onClick={() => onSave(workflowName)}>Save As</Button>
        </div>
        <div className="vertical-separator">
          <Button disabled={nodeDeleteDiabled} onClick={deleteNode}>Delete Node</Button>
          <Button disabled={edgeDeleteDiabled} onClick={deleteEdge}>Delete Edge</Button>
        </div>
        <div className="vertical-separator">
          <Button onClick={setVerticalLayout}>Vertical Layout</Button>
          <Button onClick={setHorizontalLayout}>Horizontal Layout</Button>
        </div>
        <div className="vertical-separator">
          <Button disabled={nodeDeleteDiabled} onClick={() => setTriggerActive(true)}>Configure Node</Button>
          <Button onClick={onTriggerWorkflow}>Activate Workflow</Button>
        </div>
      </div>
      <div className="container">
        <div className="leftPane">  
          <h1>Work Items</h1>
          {/* You can add other content here for the left pane */}
          <WorkItemNode metadata={{}} onClick={addSplitVideoNode}>
            Split Video
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addFrameDiffNode}>
            Frame Diff
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addActiveLabelPropNode}>
            Active Label Propagation
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addMaskRCNNInferenceNode}>
            Mask RCNN Inference
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addTrainingNode}>
            Mask RCNN Train Model
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addAnnotationNode}>
            User Annotation
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addInspectioNode}>
            User Inspection
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addDefisheyeNode}>
            Defisheye
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addMergeFramesNode}>
            Merge Frames
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addMockWorker}>
            Mock Worker
          </WorkItemNode>
          <WorkItemNode metadata={{}} onClick={addIndexVideoNode}>
            Index Video
          </WorkItemNode>
        </div>
        <div className="rightPane">
          <h1>Workflow</h1>
          <Graph  
            nodes={nodes}
            onNodesChange={onNodesChange}
            onEdgesChange={onEdgesChange}
            onConnect={onConnect}
            onNodeClick={onNodeClick}
            onEdgeClick={onEdgeClick}
            onPaneClick={onPaneClick}
            edges={edges}></Graph>
        </div>
      </div>
      <TriggerPopup 
        on={TriggerActive} 
        handleOn={toggleTriggerActive}
        associated_workflowid={guid}
        selected_node={nodeSelected}
        setNode={setNodeSelected}/>
    </div>
}

export default Workflow