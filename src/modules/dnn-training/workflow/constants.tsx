const constants = {
    MASK_RCNN_INFERENCE: "Mask RCNN Inference",
    SPLIT_VIDEO: "Split Video"
}

export default constants;