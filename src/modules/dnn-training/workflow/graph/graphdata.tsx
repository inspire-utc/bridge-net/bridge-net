
class Edge {
    id: string;
    source: string;  // UUID of source Node
    target: string;  // UUID of target Node

    constructor(source: string, target: string) {
        this.id = crypto.randomUUID();
        this.source = source;
        this.target = target;
    }
}

class Node {
    id: string;
    sources: Set<string>;  // UUIDs of source nodes
    targets: Set<string>;  // UUIDs of target nodes
    position: any;
    data: any;

    constructor() {
        this.id = crypto.randomUUID();
        this.sources = new Set();
        this.targets = new Set();
    }
}

export default
class GraphData {
    private nodes: Map<string, Node>;
    private edges: Map<string, Edge>;

    constructor() {
        this.nodes = new Map();
        this.edges = new Map();
    }

    createNode(params: any): Node {
        let node = new Node();
        node = {...node, ...params}
        this.nodes.set(node.id, node);
        return node;
    }

    deleteNode(n: Node) {
        if (!this.nodes.has(n.id)) {
            throw new Error("Node not found in the graph.");
        }

        // Remove associated edges
        Array.from(this.edges.values()).forEach(edge => {
            if (edge.source === n.id || edge.target === n.id) {
                this.edges.delete(edge.id);
            }
        });

        this.nodes.delete(n.id);
    }

    createEdge(sourceId: string, targetId: string): Edge {
        if (!this.nodes.has(sourceId) || !this.nodes.has(targetId)) {
            throw new Error("Source or Target node is not part of the graph.");
        }

        const edge = new Edge(sourceId, targetId);
        this.edges.set(edge.id, edge);

        this.nodes.get(sourceId)!.targets.add(targetId);
        this.nodes.get(targetId)!.sources.add(sourceId);

        return edge;
    }

    deleteEdge(edgeId: string) {
        const edgeToDelete = this.edges.get(edgeId);

        if (!edgeToDelete) {
            throw new Error("Edge not found in the graph.");
        }

        this.nodes.get(edgeToDelete.source)!.targets.delete(edgeToDelete.target);
        this.nodes.get(edgeToDelete.target)!.sources.delete(edgeToDelete.source);

        this.edges.delete(edgeId);
    }

    allEdges(): Edge[] {
        return Array.from(this.edges.values());
    }

    allNodes(): Node[] {
        return Array.from(this.nodes.values());
    }
}
