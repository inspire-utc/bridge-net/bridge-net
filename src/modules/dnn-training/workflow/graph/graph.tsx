import ReactFlow, { Controls, Background, MiniMap } from 'reactflow';
import 'reactflow/dist/style.css';

function Graph(props) {
  return (
    <div style={{ height: '100%' }}>
      <ReactFlow {...props}>
        <Background />
        <MiniMap />
        <Controls />

      </ReactFlow>
    </div>
  );
}

export default Graph;