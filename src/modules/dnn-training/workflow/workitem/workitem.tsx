import React from "react";
import { Paper, Grid, Typography, Button } from "@mui/material";

export default 
function WorkItemNode({ metadata:any, children, onClick}) {
    return <Paper className="workitemnode" style={{
        margin: "5px",
        padding: "5px"
    }}>
        <Button style={{fontFamily: ""}}onClick={onClick}>{children}</Button>
    </Paper>
}
