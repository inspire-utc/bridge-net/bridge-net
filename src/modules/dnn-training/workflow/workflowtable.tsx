import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableRow, TableCell } from "@mui/material";
import { Link } from "react-router-dom"
import { useNavigate } from "react-router-dom"
import axios from 'axios';
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";

const WorkflowTable: React.FC = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:8000/api/workflows');
        setData(response.data.results);
      } catch (error) {
        console.error('There was an error fetching the data!', error);
      }
    };
    fetchData();
  }, []);


  let navigate = useNavigate();

  function handleClick() {
    navigate(`/dnn-training/workflows/${crypto.randomUUID()}`);
  }


  return (
    <div className="WorkflowTable">
      <Table>
        <thead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Date Created</TableCell>
            <TableCell>Date Modified</TableCell>
          </TableRow>
        </thead>
        <TableBody>
          {data.map((workflow:any) => (
            <TableRow key={workflow.id}>
              <TableCell>
                    <Link to={workflow.id}>{workflow.name || 'Unnamed Workflow'}</Link>
                </TableCell>
              <TableCell>{new Date(workflow.date_created).toLocaleDateString()}</TableCell>
              <TableCell>{new Date(workflow.date_modified).toLocaleDateString()}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      {sessionStorage.getItem("user-info") && (
        <Fab
          sx={{
            position: "fixed",
            bottom: "5%",
            right: "5%",
            backgroundColor: "#005F83",
            color: "white",
            "&:hover": {
              backgroundColor: "#007A33",
            },
          }}
          aria-label="add"
          onClick={handleClick}
        >
          <AddIcon></AddIcon>
        </Fab>
      )}
    </div>
  );
};

export default WorkflowTable;