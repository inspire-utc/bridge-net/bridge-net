/**
 * Author: Timothy Headrick
 * Desc: This file defines everything related to the login popup where users can enter their credentials.
 * It relies heavily on materialUI, so its styles can be slightly confusing. While it has css, there
 * are many things that require using the style prop.
 */
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import logo from "../../../images/center_for_intelligent_infrastructure/center_for_intelligent_infrastructure/Logos PNG/Center for Intelligent Infrastructure_2C-P_PV.png";
import "./TriggerPopup.css"
import { Model } from 'survey-core';
import { Survey as SurveyUI } from 'survey-react-ui';
import * as Survey from 'survey-core';
import 'survey-core/defaultV2.min.css';
import { post } from "../../../utils/requests";
import { OptionsFactory } from "ag-grid-community/dist/lib/filter/provided/optionsFactory";
import constants from "./constants"
import { Label } from "@mui/icons-material";
import { nodeModuleNameResolver } from "typescript";

const DEFAULT_ELEMENT = {
  type: "panel",
  panelBase: "Hello",
  name: constants.MASK_RCNN_INFERENCE,
  title: constants.MASK_RCNN_INFERENCE,
  elements: [
    {
      name: "bridge",
      type: "dropdown",
      title: "Select Bridge",
      choices: [ "Bridge 1", "Bridge 2", "Bridge 3" ],
    },
  ]
}

/* Map the node type (based on label) to the form parameters */
const LabelToFormMap = {
  [constants.MASK_RCNN_INFERENCE]: {

    type: "panel",
    panelBase: "Hello",
    name: constants.MASK_RCNN_INFERENCE,
    title: constants.MASK_RCNN_INFERENCE,
    elements: [
      {
        name: "bridge",
        type: "dropdown",
        title: "Select Bridge",
        choicesByUrl: {
          url: "http://localhost:8000/api/bridges",
          path: "results",
          valueName: "structure_number"
        },
      },
    ]
  },

  [constants.SPLIT_VIDEO]: {
      type: "panel",
      panelBase: "Hello",
      name: constants.SPLIT_VIDEO,
      title: constants.SPLIT_VIDEO,
      elements: [
        {
          name: "bridge",
          title: "Select a Bridge",
          type: "dropdown",
          choicesByUrl: {
            url: "http://localhost:8000/api/bridges",
            path: "results",
            valueName: "structure_number"
          }
        },
        {
          name: "video",
          type: "dropdown",
          title: "Select Video",
          choicesByUrl: {
            url: "http://localhost:8000/api/videos?bridge={bridge}",
            path: "results",
            valueName: "file"
          }
        },
        {
          name: "nth_second",
          type: "text",
          title: "Extract every N Seconds",
          inputType: "number",
          isRequired: false,
        }
      ]
  }
}

 
export interface ITriggerPopupProps {
  on: boolean;
  handleOn: () => void;
  associated_workflowid: string | undefined;
  selected_node: any;
  setNode: (node: any) => void;
}

export const TriggerPopup = ({ on, handleOn, associated_workflowid, selected_node, setNode }: ITriggerPopupProps) => {

  const onSurveyComplete = (sender:any, options:any) => {
    console.log(selected_node)

    options.showSaveInProgress();
    let newNode = { ...selected_node }
    newNode.data.input_data = { ...sender.data }
    setNode(newNode)
    options.showSaveSuccess();
    handleOn();

  }
  
  /* This element gets rendered even if no node is selected */
  if (! selected_node.id)
    return <div></div>;

  let element: any = LabelToFormMap[selected_node.data.label]
  if (! element) {
    element = DEFAULT_ELEMENT
  }

  /* Restore default based on node values */
  if (selected_node.data.input_data) {
    let input_data = selected_node.data.input_data
    for (let elt of element.elements) {
      let name = elt.name
      elt.defaultValue = input_data[name]
    }
  }


  const survey = new Model({elements: [ element ]});
    
  survey.onComplete.add(onSurveyComplete)



  return (
    <>
      {on && (
        <div className="login-background">
          <div className="trigger-popup">
            <img
              src={logo}
              style={{ width: "6rem", margin: "1rem" }}
              alt="Logo"
            />
            {/*MaterialUI componentes tend to  disagree with using css for styles.
            Because of that tendency, there may be times where the style attribute needs
            to be used instead of just css.*/}
            <CloseIcon
              style={{
                color: "black",
                position: "absolute",
                right: "0.5rem",
                top: "0.5rem",
                cursor: "pointer",
              }}
              onClick={() => {
                handleOn();
              }}
            />
            <SurveyUI model={survey} />
          </div>
        </div>
      )}
    </>
  );
};
