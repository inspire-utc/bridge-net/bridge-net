import React, { useCallback, useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import constants from "./constants"


import Graph from "./graph/graph"
 // @ts-ignore
import ReactFlow, { 
  useNodesState, 
  useEdgesState,
  applyNodeChanges, 
  applyEdgeChanges,
  addEdge,

} from 'reactflow';

import axios from "axios"
import dagre from "dagre";
import { Button } from "@mui/material";
import WorkItemNode from "./workitem/workitem"
import 'reactflow/dist/style.css';
import "./workflow.css"
import { ResultPopup } from "./ResultPopup"
import { post } from "../../../utils/requests";



function Guid() {
  return crypto.randomUUID()
}



function findStartNodes(edges: any[], nodes: any[]) {
  const start_edges = edges.find((obj) => obj.source === '1')
  if (! start_edges)
    return [];
  const targets = edges.map((obj) => obj.target)
  return nodes.filter((obj) => targets.includes(obj.id))
}

const initialNodes = [
  { id: '1', position: { x: 250, y: 50 }, data: { label: 'Start' } },
];

function JobViewer() {
    /* Dynamic routing - this way we can render a workflow based on the configuration stored in a database */
    const { guid } = useParams();

    /* TriggerActive controls whether the "Activate Workflow" modal is active */
    const [ TriggerActive, setTriggerActive ] = useState(false);
    const toggleTriggerActive = () => { setTriggerActive(!TriggerActive) }

    /* dagreGraph is used purely for automatic layoutin */
    const dagreGraph = new dagre.graphlib.Graph()
    dagreGraph.setDefaultEdgeLabel(() => ({}));

    const nodeWidth = 172;
    const nodeHeight = 36;

    /* Store the state of the nodes. This is used by ReactFlow */
    const [nodes, setNodes, ] = useNodesState(initialNodes);
    const [edges, setEdges, ] = useEdgesState([]);

  
    useEffect(() => {
      const fetchData = async () => {
          try {
              const response = await axios.get(`http://localhost:8000/api/workflowjobs/${guid}`);
              const { nodes, edges, workitems } = response.data;
              
              let node_table = {}
              for (let workitem of workitems) {
                node_table[workitem.id] = workitem
              }

              for (let node of nodes) {
                if (! node.workitem_id)
                  continue
                let associated_workitem = node_table[node.workitem_id]
                if (! associated_workitem)
                  continue

                let backgroundColor = "";

                switch (associated_workitem.status) {
                  case "DONE":
                    backgroundColor = "green"
                    break
                  case "ERROR":
                    backgroundColor = "red"
                    break
                  case "PENDING":
                    backgroundColor = "yellow"
                    break;
                  case "RUNNING":
                    backgroundColor = "blue"
                    break;
                  default:
                    backgroundColor = "white"
                }

                node.style = { backgroundColor }
      
              }
              
              setNodes(nodes);
              setEdges(edges);


          } catch (error) {
              console.error('Error fetching data:', error);
          }
      };
      
      fetchData();
    }, [guid]);  // The effect will rerun if `guid` changes

    /* Store the state of start nodes. We need this to dynamically generate the form used by surveyjs */
    const [ startNodes, setStartNodes ] = useState<any[]>([]);

    /* Set the state of the start nodes */
    useEffect(() => {
      const start_nodes = findStartNodes(edges, nodes)
      setStartNodes(start_nodes)
    }, [ edges  ])

    const [ nodeDeleteDiabled, setNodeDeleteDisabled ] = useState(true);
    const [ nodeSelected, setNodeSelected ] = useState({ id: null });

    const [ edgeDeleteDiabled, setEdgeDeleteDisabled ] = useState(true);
    const [ edgeSelected, setEdgeSelected ] = useState({ id: null });


    const onNodesChange = useCallback(
      (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
      []
    );
    const onEdgesChange = useCallback(
      (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
      []
    );
    const onConnect = (params) => {
      setEdges([
        ...edges,
        { id: Guid(), ...params }
      ])
    };

    const getLayoutedElements = (nodes, edges, direction = 'TB') => {
      const isHorizontal = direction === 'LR';
      dagreGraph.setGraph({ rankdir: direction });
    
      nodes.forEach((node) => {
        dagreGraph.setNode(node.id, { width: nodeWidth, height: nodeHeight });
      });
    
      edges.forEach((edge) => {
        dagreGraph.setEdge(edge.source, edge.target);
      });
    
      dagre.layout(dagreGraph);
    
      nodes.forEach((node) => {
        const nodeWithPosition = dagreGraph.node(node.id);
        node.targetPosition = isHorizontal ? 'left' : 'top';
        node.sourcePosition = isHorizontal ? 'right' : 'bottom';
    
        // We are shifting the dagre node position (anchor=center center) to the top left
        // so it matches the React Flow node anchor point (top left).
        node.position = 
        {
          x: nodeWithPosition.x - nodeWidth / 2,
          y: nodeWithPosition.y - nodeHeight / 2,
        };
    
        return node;
      });
    
      return { nodes, edges };
    };

    const setVerticalLayout = () => {
      const {nodes: lnodes, edges: ledges} = getLayoutedElements(nodes, edges, "TB");
      setNodes([...lnodes]);
      setEdges([...ledges]);
    }

    const setHorizontalLayout = () => {
      const {nodes: lnodes, edges: ledges} = getLayoutedElements(nodes, edges, "LR");
      setNodes([...lnodes]);
      setEdges([...ledges]);
    }


    const onEdgeClick = (event, element) => {
      setEdgeDeleteDisabled(false)
      setEdgeSelected(element)
    }

    const onNodeClick = (event, element) => {
      setNodeDeleteDisabled(false)
      setNodeSelected(element)
    }
    
    const onPaneClick = () => {
      setNodeDeleteDisabled(true)
      setNodeSelected({ id: null })

      setEdgeDeleteDisabled(true)
      setEdgeSelected({ id: null })
    }




    return <div>
      <div id="Ribbon">
    
        <div className="vertical-separator">
          <Button onClick={setVerticalLayout}>Vertical Layout</Button>
          <Button onClick={setHorizontalLayout}>Horizontal Layout</Button>
        </div>
        <div className="vertical-separator">
          <Button disabled={nodeDeleteDiabled} onClick={() => setTriggerActive(true)}>View Result</Button>
        </div>
      </div>
      <div className="container">
        <div className="rightPane">
          <h1>Job Viewer</h1>
          <Graph  
            nodes={nodes}
            onNodesChange={onNodesChange}
            onEdgesChange={onEdgesChange}
            onConnect={onConnect}
            onNodeClick={onNodeClick}
            onEdgeClick={onEdgeClick}
            onPaneClick={onPaneClick}
            edges={edges}></Graph>
        </div>
      </div>
      <ResultPopup 
        on={TriggerActive} 
        handleOn={toggleTriggerActive}
        selected_node={nodeSelected}
        setNode={setNodeSelected}/>
    </div>
}

export default JobViewer