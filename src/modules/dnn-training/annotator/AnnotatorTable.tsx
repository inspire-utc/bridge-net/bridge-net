import { ROOT_URL } from "../../../utils/vars";
import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableRow, TableCell } from "@mui/material";
import { Link } from "react-router-dom"
import axios from "axios"
import { useParams } from "react-router-dom";



const AnnotatorTable = () => {
  const [bridges, setBridges] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:8000/api/bridges/')
      .then(response => {
        const bridgeData = response.data.results.map(bridge => ({
          structureNumber: bridge.structure_number,
          stateCode: bridge.state_code,
          imageCount: bridge.images.length,
          bridgeName: `${bridge.state_code}-${bridge.structure_number}`
        }));
        setBridges(bridgeData);
      })
      .catch(error => {
        console.error('There was an error fetching the bridge data', error);
      });
  }, []);

  return <div>
  <Table>
    <thead>
      <TableRow>
        <TableCell>Bridge</TableCell>
        <TableCell>Number Images</TableCell>
      </TableRow>
    </thead>
    <TableBody>
    {bridges.map((bridge:any, index) => (
          <TableRow key={index}>
            <TableCell>
                <Link to={`/dnn-training/annotations/${bridge.structureNumber}`}>
                  {bridge.bridgeName}
                </Link>
              </TableCell>
            <TableCell>{bridge.imageCount}</TableCell>
          </TableRow>
        ))}
    </TableBody>
  </Table></div>
};

export default AnnotatorTable;
