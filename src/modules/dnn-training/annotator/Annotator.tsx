/**
 * The annotator is generated server side. This is because we're using an
 * external annotator application. The server injects all the images and annotations
 * when a request is made and the result is dumped into an iframe
 */

import { ROOT_URL } from "../../../utils/vars";
import { useParams } from "react-router-dom";


const Annotator = () => {

    const { structure_number } = useParams();
    let user_info = JSON.parse(sessionStorage.getItem("user-info")!);
    let token = user_info ? "token=" + user_info.token : "";
    let structure = structure_number ? "&structure_number=" + structure_number : ""
  
    return (
      <main style={{ padding: "1rem 0" }}>
        <iframe
          title="REPLACE THIS TITLE"
          style={{ width: "100%", height: "700px" }}
          src={`${ROOT_URL}annotator?` + token + structure}
        ></iframe>
      </main>
    );
}

export default Annotator
