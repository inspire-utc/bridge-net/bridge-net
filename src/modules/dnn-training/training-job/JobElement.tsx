import { SLURM_API_URL } from "../../../utils/vars";

interface IJobElementProps {
  jobID: String;
}

const JobElement = (props: IJobElementProps) => {
  return (
    <div>
      <div>{props.jobID}</div>
      <iframe
        title="REPLACE MY TITLE"
        width="800px"
        height="500px"
        style={{ border: "none" }}
        src={`${SLURM_API_URL}/${props.jobID}/graph`}
      />
      <iframe
        title="REPLACE MY TITLE"
        width="800px"
        height="500px"
        style={{ border: "none" }}
        src={`${SLURM_API_URL}/${props.jobID}/output`}
      ></iframe>
    </div>
  );
};

export default JobElement;
