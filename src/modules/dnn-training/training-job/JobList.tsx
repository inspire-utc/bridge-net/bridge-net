import axios from "axios";
import { useState, useEffect, Fragment } from "react";
import { SLURM_API_URL } from "../../../utils/vars";
import JobElement from "./JobElement";

const JobList = () => {
  const [jobs, setJobs] = useState<string[]>([]);

  useEffect(() => {
    axios.get(`${SLURM_API_URL}`).then((data) => {
      let joblist = data.data.map((elt: any) => elt.job_id);
      setJobs(joblist);
    });
  }, []);

  return (
    <div>
      {jobs.map((elt, key) => (
        <Fragment key={key}>
          <JobElement jobID={elt} />
        </Fragment>
      ))}
    </div>
  );
};

export default JobList;
