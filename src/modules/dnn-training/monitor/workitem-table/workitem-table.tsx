import React, { useState, useEffect } from 'react';
import { Table, TableBody, TableRow, TableCell } from "@mui/material";
import { useParams } from 'react-router-dom'

const WorkItemTable = () => {
  const [workItems, setWorkItems] = useState([]);
  
  const { guid } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:8000/api/workflowjobs/" + guid);
        const data = await response.json();
        console.log(data.workitems)
        setWorkItems(data.workitems);

      } catch (error) {
        console.error("Error fetching work items:", error);
      }
    };

    fetchData();
  }, []);


  const workitemAPI = "http://localhost:8000/api" + "/workitems/"


  return (
    <Table>
      <thead>
        <TableRow>
          <TableCell>ID</TableCell>
          <TableCell>Type</TableCell>
          <TableCell>Started At</TableCell>
          <TableCell>Completed At</TableCell>
          <TableCell>Status</TableCell>
        </TableRow>
      </thead>
      <TableBody>
        {workItems.map((workItem:any) => (
          <TableRow key={workItem.id}>
            <TableCell><a href={workitemAPI + workItem.id}>{workItem.id}</a></TableCell>
            <TableCell>{workItem.type}</TableCell>
            <TableCell>{workItem.started_at ? new Date(workItem.started_at).toLocaleString() : 'N/A'}</TableCell>
            <TableCell>{workItem.completed_at ? new Date(workItem.completed_at).toLocaleString() : 'N/A'}</TableCell>
            <TableCell>{workItem.status}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default WorkItemTable;