import React, { useState, useEffect } from 'react';
import { Table, TableBody, TableRow, TableCell } from "@mui/material";
import { Link } from 'react-router-dom';


const WorkflowJobTable = () => {
  const [workItems, setWorkItems] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:8000/api/workflowjobs/");
        const data = await response.json();
        setWorkItems(data.results);
      } catch (error) {
        console.error("Error fetching work items:", error);
      }
    };

    fetchData();
  }, []);


  const workitemAPI = "http://localhost:8000/api" + "/workflowjobs/"


  return (
    
    <Table>
      <thead>
        <TableRow>
          <TableCell>Workflow Job ID</TableCell>
          <TableCell>Workflow Name</TableCell>
          <TableCell>Started At</TableCell>
          <TableCell>Completed At</TableCell>
        </TableRow>
      </thead>
      <TableBody>
        {workItems.map((workItem:any) => (
          <TableRow key={workItem.id}>
            <TableCell><Link to={workItem.id}>{workItem.id}</Link></TableCell>
            <TableCell>{workItem.name}</TableCell>
            <TableCell>{workItem.date_created ? new Date(workItem.date_created).toLocaleString() : 'N/A'}</TableCell>
            <TableCell>{workItem.completed_at ? new Date(workItem.completed_at).toLocaleString() : 'N/A'}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default WorkflowJobTable;