import axios from "axios";
import { Fragment, useEffect, useState } from "react";
import { API_ROOT_URL } from "../../../utils/vars";
import { SLURM_API_URL } from "../../../utils/vars";
import { ANNOTATED_URL } from "../../../utils/vars";
import LabeledImage from "./LabeledImage";
import "../../../styles/DNNTraining/TestSetCreator.css";

// Check box selection object
// interface IImageSelect {
//   train: object;
//   test: object;
// }

const TestSetCreator = () => {
  const [images, setImages] = useState<string[]>([]);
  //const [preview, setPreview] = useState<string>();
  const [test, setTest] = useState<string[]>([]);
  const [train, setTrain] = useState<string[]>([]);
  const [forPreview, setForPreview] = useState<any[]>([]);
  const fetchedImages = [];

  useEffect(() => {
    axios.get(`${API_ROOT_URL}images`).then((resp) => {
      resp.data.results.forEach((image: any) => {
        let label = Math.random() <= train_percent ? "Train" : "Test";
        let url = `${ANNOTATED_URL}/${image.id}`;
        //append forPreview info to forPreview state array
        setForPreview((forPreview) => [...forPreview, { label, url }]);

        if (label === "Train") setTrain((train) => [...train, image.url]);
        //append image to test to test state array
        else setTest((test) => [...test, image.url]);
      });
    });
  }, []);

  /**
   * Checking the buttons modifies the state of these objects. Not using a form because it's dynamically generated
   */
  //   let selectedTrain = Object.fromEntries(
  //     images.map((elt: any) => [elt.file, [elt.url, false]])
  //   );
  //   let selectedTest = Object.fromEntries(
  //     images.map((elt: any) => [elt.file, [elt.url, false]])
  //   );

  //   let setTrain = {
  //     setTrue: (value: string) => (selectedTrain[value][1] = true),
  //     setFalse: (value: string) => (selectedTrain[value][1] = false),
  //   };

  //   let setTest = {
  //     setTrue: (value: string) => (selectedTest[value][1] = true),
  //     setFalse: (value: string) => (selectedTest[value][1] = false),
  //   };

  //   let test: Array<string> = [];
  //   let train: Array<string> = [];
  let train_percent = 0.8;
  const onSubmit = () => {
    axios
      .post(`${SLURM_API_URL}/submit`, {
        train,
        test,
      })
      .then((result) => {
        console.log(result);
      });
  };

  return (
    <div>
      <button onClick={onSubmit}>Create Train Job</button>
      <div className="grid">
        {forPreview.map((elt: any, id) => (
          <Fragment key={id}>
            <LabeledImage label={elt.label} url={elt.url} />
          </Fragment>
        ))}
      </div>
    </div>
  );
};

export default TestSetCreator;
