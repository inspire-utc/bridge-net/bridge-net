interface ILabeledImageProps {
  label: string;
  url: string;
}

const LabeledImage = (props: ILabeledImageProps) => {
  return (
    <div>
      <div>{props.label}</div>
      <img
        className="PreviewImageData"
        src={props.url}
        alt="labeled job? (REPLACE ME)"
      />
    </div>
  );
};

export default LabeledImage;
