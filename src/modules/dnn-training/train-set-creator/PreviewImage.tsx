import { ROOT_URL } from "../../../utils/vars";

interface IPreviewImageProps {
  preview: string;
}

const PreviewImage = (props: IPreviewImageProps) => {
  let user_info = JSON.parse(sessionStorage.getItem("user-info")!);

  let token = user_info ? "&token=" + user_info.token : "";
  let url = `${ROOT_URL}annotator?preview=${props.preview}` + token;
  return <iframe src={url}></iframe>;
};

export default PreviewImage;
