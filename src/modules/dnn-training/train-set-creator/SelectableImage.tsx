interface ISelectableImageProps {
  image: any;
  onClick: any;
  getSet: any;
}

const SelectableImage = (props: ISelectableImageProps) => {
  let onClick = props.onClick;
  let boxOnClick = (event: any) => {
    event.target.checked
      ? props.getSet.setTrue(props.image)
      : props.getSet.setFalse(props.image);
  };
  return (
    <tr>
      <td>
        <button onClick={onClick}>Preview</button>
      </td>
      <td>{props.image}</td>
      <td>
        <input type="checkbox" onClick={boxOnClick} />
      </td>
    </tr>
  );
};

export default SelectableImage;
