# Step 1: Use official Node.js image
FROM node:16

# Step 2: Set working directory
WORKDIR /app

# Step 3: Copy package.json and yarn.lock
COPY package.json yarn.lock ./


# Step 5: Install dependencies
RUN yarn install

# Step 6: Copy tsconfig.json
COPY tsconfig.json ./

# Start the application
CMD ["yarn", "start"]
